<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $commitment_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property User $user
 */
class user_commitments extends Model
{

    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'commitment_id','status','progress'];
    protected $hidden = [
        'deleted_at', 'user_id',
    ];
    protected $appends = array('status_commitments');
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function commitment()
    {
        return $this->belongsTo('App\commitments','commitment_id');
    }

    public function getStatusCommitmentsAttribute()
    {
        $current = Carbon::now();
        $diff = $current->diffInHours($this->commitment->end_date, false);

        $diff_completo = $this->updated_at->diffInHours($this->commitment->end_date, false);

        if($diff < 0 && $this->status == 0){
            return "Retrasado";
        }elseif($diff >= 0 && $this->status == 0){
            return "Pendiente";
        }elseif($this->status == 1 && $diff_completo > 1){
            return "Completo";
        }elseif($this->status == 1 && $diff_completo <= 0){
            return "Completo con retraso";
        }else{
            return "";
        }
    }
}

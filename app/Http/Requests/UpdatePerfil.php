<?php

namespace App\Http\Requests;

use App\mettings;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePerfil extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation = array();


        switch ($this->method()){
            case 'GET':
                break;
            case 'POST':
                break;
            case 'PUT':
                $validation = [
                    //
                    'first_name'  => 'required',
                    'last_name'   => 'required',
                    'company'     => 'required',
                    'positions'   => 'required',
                    'address'     => 'required',
                    'city'        => 'required',
                    'state'       => 'required',
                    'cp'          => 'required'
                ];

                if ($this->file('firm'))
                    $validation = array_merge($validation, ['firm' => 'image|mimes:jpeg,png,jpg|max:2048']);
                if ($this->file('avatar'))
                    $validation = array_merge($validation, ['avatar' => 'image|mimes:jpeg,png,jpg|max:2048|dimensions:width=275,height=158']);
                break;
            case 'PATCH':
                $validation = array();
                break;
            case 'DELETE':
                $validation = array(

                );
                break;
            default:
                $validation = array();
                break;
        }
        return $validation;
    }

    public function attributes()
    {
        return [
          'first_name' => 'nombre',
          'last_name' => 'apellido',
          'company' => 'institución',
          'positions' => 'cargo',
          'address' => 'dirección',
          'city' => 'ciudad',
          'state' => 'estado',
          'cp' => 'codigo postal',
          'firm' => 'firma electronica',
          'avatar' => 'imagen de perfil'
        ];
    }
}

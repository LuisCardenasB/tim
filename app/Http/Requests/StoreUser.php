<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation = array();


        switch ($this->method()){
            case 'GET':
                break;
            case 'POST':
                $validation = [
                    //
                    'email' => 'required|unique:users,email',
                    'first_name'  => 'required',
                    'last_name'  => 'required',
                    'password'  => 'required',
                    'password_confirmation'  => 'required',
                ];

                if ($this->file('firm'))
                    $validation = array_merge($validation, ['firm' => 'image|mimes:jpeg,png,jpg|max:2048']);
                break;
            case 'PUT':
                $validation = array();
                break;
            case 'PATCH':
                $validation = array();
                break;
            case 'DELETE':
                $validation = array(

                );
                break;
            default:
                $validation = array();
                break;
        }
        return $validation;
    }

    public function attributes()
    {
        return [
          'firm' => 'firma electronica',
          'password_confirmation' => 'confirmar contraseña'
        ];
    }
}

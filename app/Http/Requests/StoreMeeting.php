<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMeeting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation = array();


        switch ($this->method()){
            case 'GET':
                break;
            case 'POST':
                $validation = [
                    //
                    'date' => 'required',
                    'title'=> 'required|max:250',
                    'users' => 'required|array',
                    'location' => 'required',
                    'owner' => 'required'
                ];
                break;
            case 'PUT':
                $validation = array();
                break;
            case 'PATCH':
                $validation = array();
                break;
            case 'DELETE':
                $validation = array(

                );
                break;
            default:
                $validation = array();
                break;
        }
        return $validation;
    }

    public function attributes()
    {
        return [
          'title' => 'Título de la reunión',
          'users' => 'Participantes',
          'location' => 'Lugar',
          'owner' => 'Responsable de la reunión'
        ];
    }
}

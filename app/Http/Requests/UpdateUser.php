<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation = array();


        switch ($this->method()){
            case 'GET':
                break;
            case 'POST':
                break;
            case 'PUT':
                $validation = [
                    //
                    'email' => 'unique:users,email',
                    'first_name'  => 'required',
                    'last_name'  => 'required',
                ];

                if ($this->file('firm'))
                    $validation = array_merge($validation, ['firm' => 'image|mimes:jpeg,png,jpg|max:2048']);
                break;
            case 'PATCH':
                $validation = array();
                break;
            case 'DELETE':
                $validation = array(

                );
                break;
            default:
                $validation = array();
                break;
        }
        return $validation;
    }

    public function attributes()
    {
        return [
            'firm' => 'firma electronica',
            'first_name' => 'nombre',
            'last_name' => 'apellido',
            'email' => 'correo electronico'
        ];
    }

    public function messages()
    {
        return [
            'email.unique' => 'El correo electronico ya se encuentra registrado'
        ];
    }
}

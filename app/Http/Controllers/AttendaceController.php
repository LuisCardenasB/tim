<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\mettings_attendance;
class AttendaceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get_by_metting(Request $request, $id){
        $attendences = mettings_attendance::where("mettings_id",$id)
            ->update(['status' => 0]);
        $raw = 'mettings_id = '.$id.' AND user_id IN ('.implode(",",$request->attendaces).')';
        $attendences = mettings_attendance::whereRaw($raw)
            ->update(['status' => 1]);
        return response()->json(array("status"=>true));
    }
    public function add_attendance_metting(Request $request, $id){
        foreach ($request->attendaces as $key => $value){
            $attendance = new mettings_attendance();
            $attendance->user_id = $value;
            $attendance->mettings_id = $id;
            $attendance->save();
        }
        return response()->json(array("status"=>true));
    }
}

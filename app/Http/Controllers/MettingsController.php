<?php

namespace App\Http\Controllers;

use App\Revision;
use App\RoleUser;
use App\User;
use App\mettings_attendance;
use App\UserProfile;
use HttpOz\Roles\Models\Role;
use Illuminate\Http\Request;
use App\mettings;
use App\Http\Requests\StoreMeeting;
use Illuminate\Support\Facades\Mail;
use Session;
use Illuminate\Support\Facades\Redirect;
use Notification;
use App\Notifications\minutes;
use App\user_commitments;
use App\commitments;
use DB;
use App\Mail\SendMinutes;
use App\Attachment;
use Auth;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Thinreports\Report;

class MettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isAdmin()){
            $meetings = mettings::where('deleted_at', null)->orderBy('date','desc')->paginate(20);
        }else{
            $meetings = mettings::join("mettings_attendance","mettings.id","=","mettings_attendance.mettings_id")
                ->where("mettings_attendance.user_id",Auth::user()->id)
                ->where("mettings.deleted_at", null)
                ->select("mettings.*")
            ->orderBy('date','desc')->paginate(20);
        }

        $role_user = RoleUser::where('user_id', Auth::user()->id)->first();
        $users = User::all();

        foreach ($users as $user)
        {
            $selects[$user['id']] = $user->email;
        }

        return view('mettings.list')
            ->with('role_user',$role_user->role_id)
            ->with('users', $selects)
            ->with('meetings', $meetings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users   = User::all();
        $revision = Revision::where("status",1)->get()->first();
        foreach ($users as $user) {
            $selects[$user['id'] ] = $user->profile->prefix.' '.strtoupper($user->profile->first_name).' '.strtoupper($user->profile->last_name);
        }
        return view('mettings.create')
            ->with('revision',$revision)
            ->with('users', $selects);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMeeting  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMeeting $request)
    {
        $input = $request->all();
        $meeting = new mettings();
        $meeting['minutes'] = $input['agenda'];
        $meeting->fill($input);
        if ($meeting->save()){
            $id = $meeting->id;
            foreach ($input["users"] as $item){

                $user =  new mettings_attendance();
                $user->user_id = $item;
                $user->mettings_id = $id;

                $user->save();

            }
            Session::flash('message', 'La reunión ha sido agendada.');
            return Redirect::to('mettings');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $metting = mettings::find($id);
        if (!$metting){
            Session::flash('message', 'No existe la reunión');
            return Redirect::to('mettings');
        }
        $attendances   = mettings_attendance::where("mettings_id",$id)->join("user_profiles","user_profiles.user_id","=","mettings_attendance.user_id")->get();
        $commitments = user_commitments::with(['commitment','commitment.metting','user'])->whereHas('commitment', function ($query) use ($id) {
            $query->where('metting_id', '=', $id);
        })->get();
        $users  = DB::select(DB::raw("SELECT users.id, users.name FROM `users` INNER JOIN user_profiles ON users.id = user_profiles.user_id WHERE users.id  NOT IN (SELECT user_id FROM mettings_attendance WHERE mettings_attendance.mettings_id = :id)"),[$id]);

        return view('mettings.show')
            ->with('metting',$metting)
            ->with('commitments',$commitments)
            ->with('users',$users)
            ->with('attendances', $attendances);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $metting = mettings::find($id);
        $revision = Revision::where("status",1)->get()->first();
        //Validamos si la reunion se encuentra en un estado cerrado
        if ($metting->status == 3){
            Session::flash('message', 'La reunion se encuentra cerrada, no se puede modificar.');
            return Redirect::to('mettings');
        }
        $selects = array();
        $users   = mettings_attendance::where("mettings_id",$id)->join("user_profiles","user_profiles.user_id","=","mettings_attendance.user_id")->get();

        //dd($users);
        foreach ($users as $user) {
            $selects[$user['user_id'] ] = $user->prefix.' '.strtoupper($user->first_name).' '.strtoupper($user->last_name);
        }

        $penultimate_id_metting = mettings::orderby('created_at', 'DESC')->offset(1)->limit(2)->first();

        $past_commitments = commitments::select(['users.name','commitments.descriptions','commitments.end_date','user_profiles.avatar','user_commitments.progress', 'user_commitments.status'])
            ->where("metting_id",$penultimate_id_metting->id)
            ->orWhere("user_commitments.status", 0)
            ->join("user_commitments","user_commitments.commitment_id","=","commitments.id")
            ->join("users","user_commitments.user_id","=","users.id")
            ->join("user_profiles", "user_profiles.user_id", "=", "user_commitments.user_id")
            ->get();

        return view('mettings.edit')
            ->with("users",$selects)
            ->with("attendence",$users)
            ->with('revision',$revision)
            ->with("metting",$metting)
            ->with("past_commitments",$past_commitments)
            ->with("title_metting",$penultimate_id_metting->title);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\StoreMeeting  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMeeting $request, $id)
    {
        //
        $input = $request->all();
        $metting = mettings::find($id);
        $metting->fill($input);
        if ($metting->save()){
            Session::flash('message', 'Reunion Actualizada!');
            return Redirect::to('mettings');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $metting = mettings::find($id);

        $metting_attendance = mettings_attendance::where('mettings_id',$id)->get();
        foreach ($metting_attendance as $attendance)
        {
            $attendance->delete();
        }

        $commitments = commitments::where('metting_id', $id)->get();
        foreach ($commitments as $commitment)
        {
            $user_commitments = user_commitments::where('commitment_id', $commitment->id)->get();
            foreach ($user_commitments as $user)
            {
                $user->delete();
            }
            $commitment->delete();
        }

        if ($metting->delete()):
            Session::flash('delete', 'La reunion ha sido eliminada');
            return Redirect::to('mettings');
        endif;

    }
    public function close_meeting(StoreMeeting $request, $id){
        $input = $request->all();

        $metting = mettings::find($id);
        $input["status"] = 3;
        $metting->fill($input);

        if ($metting->save()){
            Session::flash('message', 'Reunion Cerrada!');
            $users   = User::join("mettings_attendance","users.id","=","mettings_attendance.user_id")->where("mettings_id",$id)->get();
            foreach ($users as $user){
                Mail::to($user->email)
                    ->send(new SendMinutes($id));
            }
            return response()->json(array("status"=>true),200);
        }
    }
    public function update_meeting(StoreMeeting $request, $id){
        $input = $request->all();

        $metting = mettings::find($id);
        $metting->fill($input);

        if ($metting->save()){
            Session::flash('message', 'Auto guardado!');
            return response()->json(array("status"=>true),200);
        }
    }
    public function sendMail($id)
    {
        $users   = User::join("mettings_attendance","users.id","=","mettings_attendance.user_id")->where("mettings_id",$id)->get();
        foreach ($users as $user){
            Mail::to($user->email)
                ->send(new SendMinutes($id));
        }

        Session::flash('message', 'Minuta enviada');
        return Redirect::to('mettings');
    }
    public function upload(Request $request,$id){
        if ($request->hasFile('file')){
            $attachment = new Attachment();
            $attachment->name = $request->file('file')->getClientOriginalName();
            $attachment->path = $request->file('file')->store('attachments', 'public');
            $attachment->user_id = Auth::user()->id;
            $attachment->mettings_id = $id;

            if($attachment->save()){
                return response()->json(["status"=>true]);
            }

        }
    }
    public function download($id){
        $attachment = Attachment::find($id);
        return response()->download(
            storage_path('app/public/'.$attachment->path),
            $attachment->name,
            [],
            ResponseHeaderBag::DISPOSITION_INLINE
        );
    }
    public function metting_print($id){
        $metting = mettings::find($id);
        if (!$metting){
            Session::flash('message', 'No existe la reunión');
            return Redirect::to('mettings');
        }
        $attendances   = mettings_attendance::where("mettings_id",$id)->where("status",1)->join("user_profiles","user_profiles.user_id","=","mettings_attendance.user_id")->get();
        $users  = DB::select(DB::raw("SELECT users.id, CONCAT(prefix,' ',first_name,' ',last_name) AS name FROM `users` INNER JOIN user_profiles ON users.id = user_profiles.user_id WHERE users.id  NOT IN (SELECT user_id FROM mettings_attendance WHERE mettings_attendance.mettings_id = :id)"),[$id]);

        $report = new Report(storage_path('app/layouts') . "/CMIC.tlf");
        $page1 = $report->addPage();
        $page2 = $report->addPage(storage_path('app/layouts') . "/CMIC3.tlf");
        //$page1->setItemValue("comite","Comité Directivo");
        //$page1->setItemValue("folio",$metting->id);
        //$page1->setItemValue("hora",\Carbon\Carbon::parse($metting->date)->format('h:i A'));
        //$page1->setItemValue("lugar",$metting->location);
        //$page1->setItemValue("duracion",\Carbon\Carbon::parse($metting->updated_at)->diffForHumans(\Carbon\Carbon::parse($metting->date)));

        $participantes = "<table border=\"1\" style=\"border-collapse: collapse; font-size: 10px; padding: 10px 5px 10px 10px; margin-bottom: 15px;\"><tr ><th colspan=\"3\" align=\"center\"><strong>".trans('metting.participants_PDF')."</strong></th></tr><tr><td>".trans('metting.name_PDF')."</td><td>".trans('metting.job_PDF')."</td><td>".trans('metting.firm_PDF')."</td></tr>";
        foreach ($attendances as $key => $value){
            $participantes .= "<tr>";

            $img = '';

            if($value->firm){
                $imagedata = file_get_contents(asset('storage/').'/'.$value->firm);
                $type = pathinfo(asset('storage/').'/'.$value->firm, PATHINFO_EXTENSION);

                // alternatively specify an URL, if PHP settings allow
                $base64 = base64_encode($imagedata);
                $img = '<img src="data:image/' . $type . ';base64,' . $base64 .'" style="height:45px; width:135px" >';
            }

            if($value->status == 1){
                $participantes .=  "<td> $value->prefix $value->first_name $value->last_name</td><td>$value->positions</td><td>".$img."</td>";
            }else{
                $participantes .= "<td></td><td></td><td></td>";
            }
            $participantes .= "</tr>";
        }
        $participantes .= "</table><p></p>";


        $contenido = $participantes;

        $contenido .= "<table border=\"1\" style=\"border-collapse: collapse; font-size: 10px; padding: 10px 5px 10px 10px\">
                            <tr>
                                <th align=\"center\"><strong>".trans('metting.development_comments_PDF')."</strong></th>
                            </tr>
                            <tr>
                                <td>
                                $metting->agenda
                                </td>
                            </tr>
                            <tr>
                                <th>".trans('metting.metting_PDF')."</th>
                            </tr>
                            <tr>
                                <td>
                                $metting->minutes
                                </td>
                            </tr>
                      </table><p></p>";

        $acuerdos = "<table border=\"1\" style=\"border-collapse: collapse; font-size: 10px; \"><tr ><th colspan=\"4\" align=\"center\"><strong>".trans('metting.agreements_PDF')."</strong></th></tr><tr><td>".trans('metting.activity_PDF')."</td><td>".trans('metting.responsible_PDF')."</td><td>".trans('metting.commitment_date_PDF')."</td><td>".trans('metting.tracking_date_PDF')."</td></tr>";

        foreach ($metting->commitments as $item){
            $acuerdos .= "<tr><td>$item->descriptions</td>";
            $acuerdos .= "<td><ul>";
            $users  = DB::select(DB::raw("SELECT CONCAT(user_profiles.first_name,' ', user_profiles.last_name)name FROM `users` INNER JOIN user_profiles ON users.id = user_profiles.user_id INNER JOIN user_commitments ON user_commitments.user_id = users.id WHERE users.id and user_commitments.commitment_id = :id"),[$item->id]);
            foreach ($users as $user){
                $acuerdos .= "<li><i>$user->name</i></li>";
            }

            $acuerdos .= "</ul></td>";
            $acuerdos .= "<td>".\Carbon\Carbon::parse($item->end_date)->format('d-m-Y')."</td>";
            $acuerdos .= "<td>".\Carbon\Carbon::parse($item->tracking_date)->format('d-m-Y')."</td>";
            $acuerdos .= "</tr>";

        }

        //DOCUMENTOS ENTREGADOS
        $acuerdos .= "</table><p></p>";

        $acuerdos .= "<table border=\"1\" style=\"border-collapse: collapse; font-size: 10px; \"><tr ><th  align=\"center\"><strong>".trans('metting.documents_delivered_PDF')."</strong></th></tr>";
        for($i=0; $i < 3; $i++){
            $acuerdos .= "<tr><td></td></tr>";
        }
        $acuerdos .= "</table><p></p>";

        //OBSERVACIONES

        $acuerdos .= "<table border=\"1\" style=\"border-collapse: collapse; font-size: 10px;  \"><tr  ><th bgcolor='#FFFF00' align=\"center\" ><strong>".trans('metting.observations_PDF')."</strong></th></tr>";
        for($i=0; $i < 1; $i++){
            $acuerdos .= "<tr style='heigth:15px;'><td></td></tr>";
        }
        $acuerdos .= "</table>";

        $image = '<table>
                      <tr>
                        <th style="margin-left: 50px;"><img  width="161" height="54.7" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAASQAAABkCAMAAAAYCL0GAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAABvUExURQAAAHNzc3Nzc5sdAHNzc1AoIT4wLnNzc0kuKaEeAHFxcSIKBp0eAHV1dZkdAKQfAJcdAHZ2dnNzc6IfAHh4eFoRAJ4eAKogAKsgAKAeAHR0dF0RAHh4eKYfAJ4eAFgRAJ4eAGpqalYQAJ0eAKsgALFAtzwAAAAjdFJOUwBAwG+BGgf+EPQtKVfo7Zsw07OEm5Hgt9erVz1rQL1i0HJ8C8aPTQAADVRJREFUeNrtXAuXojoM5tUHCCLI4Migg+v8/994m5ZHgb7wjrvr2ck543CwhORr8iUtquc9Qw7Hg/fyQsP68jztaXe/h68O0SG/3+/HJylHx/39iep/UxDBPD/Ni8v1LuT8whD188zk+gTlYXe/P0/9b5L0Gj/Pi0M+Kb/f9y8aRGEne3H/VuU4nSv/ZvW/jawXTnynF/RYL7XfkdYSJ0G/HaH1PDMZe4BDuFXm7UN6jtfa7xc1mt0+dpP9NZXToN4rpE61Ll9V400UE9YKJyYv6niryLZdOpXy+11lf1qrx6olvuLJBc0QXc96UA7P9Wytsexo1GcUyTSkc1vRYqB4432mmT9rRtRYExfK0doO92LzIt2MUTxZhjvdIMWsnTffaYx27W00/djVrG4ptW2ajptNlww7OkTBGEjbQ3YMR/0QdXSox2rCzjB7VyPoJgntYcoSYZ33j4NkoAQlLanHdzpCMmjvh8SbTUf2MFW1GMfHQQoNY2pnStKsxIxU+ShvT1blblA+HLJjzOaO2W82LN3CX7MatT0JRkpOYyfSNfLE1yBGHVfH9Dfz/GFDIZxbsD0J0rHBcxpm4onTYTfIJe30TGuptgenErFX8vYhdpiA7XUZu1X00D5jezkl8Rr0zrGVWzLyxbHeGpsLmce+ZqIZKg85ORar3M4TuYXZc9e6mLuUCCVv2xJJILubidKcfSoP6Wcf7Z3bZS2vpBaDU70n9QzzOHWgYhVvf8QbvdDOWY631gRVDijMoZae7qBP7PN8kmaJq+lqFBsLuLOuLlyjLzXXhKtTo3Swdzi1jmljFeMdtFNycO6npnDch67tnoaMFaXzMM1jfnFqlFJrg4N1oY7UpTnV0VLo2k5JPUx6sC/mTYEaY1NNqBF2Up9bmfSyYQkONuFZ2McflqXYmrelcnrGeO/Y7qkbjM6YlRdtFU1tpXZx/8v7XE4HPQdwm+gMjZqaKeliSBugtOvdke5Tt00PqSYc9YuG0JhL60U53S3EEBnCJjRD/mTebVjlw+ccCRcv9NmcGmpCR/VtTG7h7c519znW2nTYKxIqddsCkJLthB292MDb+SKGDw4tRrhlM9XOAUgV+j0t5W7Onhb9A3VtlGqXOE3jxdxpOK+z8LbrxwVSUzkP17RUO6XNcRWWtVvnQF3ilNbzZNM2lvE23tbK0VTOcb48T51KuZSoJ2xci3ouC8Pckmz6BZAUgtjeb2vlbAwL9L544+LE29d1sx667Yu58Haq6DxS6+T9H97em4NwJ6fJ/kMzY502OkPz3sEq4HM7klJNmBoTalX/zbw9C8KLjOI77Rx4W+phTtjiReqwt7LXxv7F9kgmfBZvd4YEODm4KgG5323wQkccixIYqtt860JgA28jEDNvL4IQO+yuIk3GhNad8dzhQcxsjLQ/0VHrPvF5C2+jNyHnK8j57Q3pJ2CZATTftOOhTDYttV638vZJEwi251wO+yThfG/06wvpeXu1MzQvcRZPpcHxzu5FvZG3j7o1tXojYG+C/2wp9LWetxU7Qx97x0fB8zvN+UbTFNs3G2N1mHbUZTsdu+9vrwLmrM8B5ZZq7ProRlJ4wi67rcjWkMgG0Xf9Q5zc3Ch1Vl5Burk/Ou7oh468LfcwyOmpwMW6mZQrVeR0IalRvYq3D5ZCn+p5W1kXjSVur6TV88dCjvbuITWPkB8sXK+nuVyNLcbhbi1uuW7u967rGXp2eRIuB1y92OV7d2iUjsY4tRYQA12G9nVjp+FCN962WRi6Ery1CTob4zTfrn2cwNy+2ok1XHjZsJ75qM1P9OfJ5ixX22bS9X98uGhqlFTJmCNjtuX6INR/mlRb4ujjn/6ZRy4yJYzlwxEWvlTG+Ont7ddnL/l9A2+jTVtPspuz57VfCrHuKF1MvC3ZGp9yhZxMjZL63syo4eO8eiRq24rbvms4pj2WSOv9+Eshua1RCg3vy++FiCpktzc0So/HIHJrJS2flQxXcVZ/qEG+WholA2/LCOi+3WRS/zjlXxxbSXMkHVa6dLxmW74aeFvKpSveQgfhoyCleqMv2/aeRt6Wd+O0QF/MjRLV87Zkab3bZN/xwfbhhB/ibc2N6uV72pm2rToO2gndxS7lFxmyZmsjet3pl5P1xu3wwYrUZaYtfZCet3Hnxgem5fFH/h5/OUr8nk9OfK2+12LaGqdf2q/CyD1Muu3RjLQvdnxfy2lBNh02qL8a+7CPz19vbvIpF57PlexMzww+lULnzhu/+RoaG6UPhezmVLbfbXyG+Pd8NTCUn6eYBh5UwW3TLtcE8/ON3fm0kvwvwUjuYT7M/qqCe8MzZtsXtPG6xfxLMMInN1p9UKSa8I68V5Vw/SGmbxSpJsQfL4uR1MPUT5jpfPsD179P5L359KnJdnpZjGZ780+tCfXrEpLUw1y/v5LINSF93WSbaHX/hN8rCp8apn8g2cKnJtvr/jqLTKtP+CEeenpqmP4mkXqYd/rUZHvd6i997HT/hD7v8tQw/QOt9hNmmnbjbsyJvixGtKt7eX/GTKO34SnL5+ti5KF0kNddU/3Ij/zIj/zIj/zIvysY/bGtKulBBd3tdP0gkr+RhqVfatM/Kze2lhh7DjrmhjZV5W9sEW3tLRUjsG1cE8FgsBO3ZemLw5VUiXRF2wxTitpWY3dStSbj2HXDfZK2TVw8bkmWlWrEvUdsYNb7UdXu2NV+Zbl3FAAG4GqT+b7vqWeLSGcJIYPSkpBCo5aUhpv6TEfT45URErmAlBHqKfMtaekjNni4zG63MmMzdMtcQMpu4DF3nR9aQCoyMXmUFCTQzFKTGEEqBgcadugEktbjiPiP2MCMAHATAJ66gMQHBdxW9fg5SG0fBjd2FAhO3e2AtkSOY3gVaYuGXwiC3zuYiM0nQUnEncos4iBxFbRnEjRchbxeH0IkQEwBHYeJQ4xRRBqgdHGWcjNAAxI2YBgykC1cMX5HLgqmkuD1F8G4YTSdLhQgUaY9iPi37OjokzcqXoBExazirPA5SLgpWHixqG8K8NcvGi8pgA8Slo9ZyeYTVSyrsjEv2GUNuYlgjDhIuC0yknHqK1qfXQZXeRVPZtBVwR2KhEbsfxYB2qCSGZDAdQUbV/BEaAt2XVUlEKnchqTwW3ZNBbcGvUVR9Axxy4bsTeBMWyUVs4CKV7h525T9oeCk1mtKxotMuHOI2cSdA8UZs30OkifCwCeNAKklpc90Rx4uSOLRIqPsPeY5ykh5i7IIiKe83aa8YpfhjOdsRBAHKSHF7RZwsiMBG1xysguIGB157AQzD7UZ19MCo5CAHQaoZClflnDZmHtBkRXsFLfBJyWJhGaaZT6HYGDDso9tH24TFQGb26wqbsxuGBJkjK9wUA0gsUyjSVElTCAWcBmwtyNAGsGNmFlzkBrubkkwBwkTTnwFwUA2NIJk5AZW3GmUMBT5AelTDEBib/oQjIHHQUK+IGeu/8a1URmkHgTOIAkEciNKhg+48NmUQRLHPUhwhsLoFsD1irHU+FkWcZgESATx+QYmaTMOkrhZMoE0cBK8NoLtAa+qFOQzB4klGqMH5jkHqSEV5Ck3lsUTz0VuYEF65uwPqqGkwWUJANewM1EfYJhSDorwvvd2ARIfheGwIu2cuGcgIQkk4RYZBgqdIpairGjwAFIwBpV4DUQJy1o1SFXFPw4DABWtgrg9HgYt++MgRUAKBeMGnwcDB4TbRoh0CfcgmkDikceCsT9LWdNSiUgKDCAh1lJWcBiMJqlA8lQgNaTFmM6aFlyJsFeCJIyFfyqQgiACCcoRnSVIEPEQtz1IjChAoJgwGqVuILHoh2AUZymQYGkFyc8IY5sHQWKcxLg4a+ZtcvEoSL3TvhYkSCBhAvd26lgh3QI8gkSnRnCZbixdAwhGAVLEdQQ2kAquAQ7LQZc7SC2JGMbNsluygFRo0q2curvelBVIN8Id594mInhgDQYRVkrE3ULXkgwHmUzc4GdWDPHFAwMvQSohdWk5gcTvTcXMlBC4yQgSr5aoMIAUFFhexQhbgH81IMEN9MTdFmPbJ9pwfwUSJT0/C2/LhCZtBS0A67jI0AKwytJQqJfioBwjTlzWiDoWCTwjSluyAIn1qknCklCOJMaVPA7ZWdbT8LpRUp+XUpoIYtSAdONNUtXgYT0K7SKr+DqQ2A0whe5CCRJmbR+juCQBoFnH6mcrkPpw9zlUDBuQiDl141EW9bZFcBpWRw0fUOIRJH6ZiCzBSRl4EJFx/cFB4oqLRIAkJoUEWcUNbzIiVoAYDtiUw7/SFwG4AqkUnMT6wHGpyKg0qrJybAHKVboVQRRAcyre4/klkoy/JhmjbkFxVRZVxSySEl7OqajuiQja5HZrKG+J+hGYdzRwXiz3aXu7TSspLC4TmnDC9yCaWyOuF2cRP+uxs7jvjpJejz8dNmKno+WqMbzFlYn3hQ2DIQmERiKmZtx8idreAm+wHU+vLFyaSISdsAsNL/0r9qOo38zwI8Yniduez98t/TqzyNyGB5H3DwpbzjCSiUj0A5JekoBTpKvv/yZIwCJ+4o4p8n7kGfIf118dhfzv3nEAAAAASUVORK5CYII="></th>
                      </tr>
                    </table><p></p>';
        $fecha = '<table border="1" style="border-collapse: collapse; font-size: 10px;  padding: 10px 5px 10px 10px ">
                    <colgroup>
                    <col style="width: 80px">
                    <col style="width: 211px">
                    <col style="width: 80px">
                    <col style="width: 211px">
                    </colgroup>
                      <tr>
                        <th style="width: 75px"><span style="font-weight:bold">'.trans('metting.affair_PDF').'</span></th>
                        <th style="width: 315px">'.$metting->title.'</th>
                        <th style="width: 75px"><span style="font-weight:bold">'.trans('metting.date_PDF').'</span></th>
                        <th style="width: 90px">'.\Carbon\Carbon::parse($metting->date)->format('d/m/Y').'</th>
                      </tr>
                    </table><p></p>';

        $page1->setItemValue("orden",$image.$fecha.$contenido);
        //$page2->setItemValue("responsable",$metting->owner);
        $page2->setItemValue("acuerdos",$acuerdos);
        $report->generate('/preview.pdf', 'I');
        /*return view('mettings.print')
            ->with('metting',$metting)
            ->with('commitments',$commitments)
            ->with('users',$users)
            ->with('attendances', $attendances);*/
    }
}

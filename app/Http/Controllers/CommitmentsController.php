<?php

namespace App\Http\Controllers;

use App\commitments;
use App\Mail\NewCommitmentsComplete;
use App\Mail\notification_commitment;
use App\Mail\notification_host;
use App\Mail\SendMinutes;
use App\User;
use App\user_commitments;
use Illuminate\Http\Request;
use App\Notifications\compromiso;
use Auth;
use Illuminate\Support\Facades\Mail;
use Session;
use Redirect;
use Datatables;

class CommitmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $commitments = user_commitments::select(['user_commitments.id','user_commitments.user_id as owner','users.name', 'user_commitments.status', 'commitments.descriptions', 'user_commitments.progress', 'commitments.end_date', 'mettings.title'])
                ->join("commitments","user_commitments.commitment_id","=","commitments.id")
                ->join("mettings","commitments.metting_id","=","mettings.id");


        if(!Auth::user()->isAdmin()) {
            $commitments = user_commitments::with(['commitment','commitment.metting','user'])->where("user_id", Auth::user()->id)->orderBy('id', 'DESC');
        }else{
            $commitments = user_commitments::with(['commitment','commitment.metting','user'])->orderBy('id', 'DESC');
        }
        //return Datatables::of($commitments)->make(true);
        return view("commitments.index")->with("commitments",$commitments->paginate(100));
    }

    public function getData(){
        $commitments = user_commitments::select(['user_commitments.id','user_commitments.user_id as owner','users.name', 'user_commitments.status', 'commitments.descriptions', 'user_commitments.progress', 'commitments.end_date', 'mettings.title'])
            ->join("commitments","user_commitments.commitment_id","=","commitments.id")

            ->join("mettings","commitments.metting_id","=","mettings.id");
        if(!Auth::user()->isAdmin()) {
            $commitments->where("user_commitments.user_id", Auth::user()->id);
        }else{
            $commitments->join("users","users.id","=","user_commitments.user_id");
        }
        return Datatables::of($commitments)->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $input = $request->all();

        $commit = new commitments();

        $commit->fill($input);

        if ($commit->save()){
            $id_commitment = $commit->id;
            $metting_id = $commit->metting_id;
            foreach ($input["users"] as $commit){
                $user =  new user_commitments();
                $user->user_id = $commit;
                $user->commitment_id = $id_commitment;
                $user->status = 0;
                $user->save();
                // USUARIO AL QUE SE LE ENVIA UN CORREO ELECTRONICO
                $this->SendEmailUserCommitments($user->user_id,$id_commitment,$metting_id);
                //USUARIO AL QUE SE LE ENVIA LA NOTIFICACION
                $user = User::find($user->user_id);
                $user->notify(new compromiso($commit));
            }
            return response()->json(array("status" => true),200);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $commit = user_commitments::find($id);

        if ($commit->user_id != Auth::user()->id){
            Session::flash('message', 'Usuario no autorizado');
            return Redirect::to('commitments');
        }
        $commit->status = 1;
        if ($commit->save()){
            Session::flash('message', 'Se ha actualizado el compromiso');
            $this->SendEmailHost($id);
            return Redirect::to('commitments');
        }
        Session::flash('message', 'No se pudo actualizar el compromiso');
        return Redirect::to('commitments');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function avance(Request $request){
        $commitments = user_commitments::find($request->pk);
        if ($commitments){
            $commitments->progress = $request->value;
            $commitments->save();
            return response()->json(["body"=>$commitments],200);
        }
        return response()->json(["error"=>"Existe un error"]);
    }

    public function commitments_edit(Request $request)
    {
        $commitments_edit = commitments::find($request->pk);

        if ($request->name == "description") {
            $commitments_edit->descriptions = $request->value;
            $commitments_edit->save();
        }

        if($request->name == "date") {
            $date = date('Y-m-d', strtotime($request->value));
            $commitments_edit->end_date = $date;
            $commitments_edit->save();
        }

        if($request->name == "date_tracking") {
            $date_tracking = date('Y-m-d', strtotime($request->value));
            $commitments_edit->tracking_date = $date_tracking;
            $commitments_edit->save();
        }
    }

    public function SendEmailHost($id)
    {
        $commitments = user_commitments::join("commitments", "user_commitments.commitment_id", "=", "commitments.id")
            ->join('mettings', 'commitments.metting_id', "=", "mettings.id")
            ->join('users', 'mettings.user_id', '=', 'users.id')
            ->select('users.email','mettings.id','commitments.id as compromiso_id')
            ->where("user_commitments.id", $id)->first();
        $emails_cc = explode(",",config('mail.email_cc'));

        Mail::to($commitments->email)
            ->cc($emails_cc)
//            ->cc(['julopez@tacna.net', 'amedina@tacna.net', 'dmurillo@tacna.net'])
            ->send(new notification_host($commitments->id, $commitments->compromiso_id));
    }

    public function SendEmailUserCommitments($id_user, $id_commit, $metting_id) {
        $user_commitments = User::where("id", $id_user)->select('email')->first();

        Mail::to($user_commitments->email)
            ->send(new notification_commitment($id_user,$metting_id,$id_commit));
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\user_commitments;


class NotificationsController extends Controller
{
    //
    public function get_by_user(){
        $response = array();
        $commitments   = user_commitments::select()
            ->join("commitments","commitments.id","=","user_commitments.commitment_id")
            ->where("user_id",Auth::user()->id)
            ->where("status",0)
            ->get();
        $response= $commitments;
        return response()->json($response,200);
    }
}

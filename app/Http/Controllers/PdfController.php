<?php

namespace App\Http\Controllers;

use App\mettings;
use App\mettings_attendance;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PdfController extends Controller
{
    public function invoice($id)
    {
        $users_acuerdos=[];

        $metting = mettings::find($id);
        if (!$metting) {
            Session::flash('message', 'No existe la reunión');
            return Redirect::to('mettings');
        }

        $texto[] = $metting->minutes;

        foreach ($texto as $item) {
            $explode = explode("</p>",$item);
            for ($i=0; $i<count($explode); $i++)
            {
                $value = $explode[$i];
                $array [] = $value . '<br/>';
            }
        }

        $metting->agenda = strip_tags('<p>'.$metting->agenda.'</p>');
        foreach ($array as $minutes)
        {
            $minuta[] = strip_tags('<p>'.$minutes.'</br>');
            $minuta = str_replace("&nbsp;", '', $minuta);
        }

        $attendances = mettings_attendance::where("mettings_id", $id)->where("status", 1)->join("user_profiles", "user_profiles.user_id", "=", "mettings_attendance.user_id")->get();
//        $users = DB::select(DB::raw("SELECT users.id, CONCAT(prefix,' ',first_name,' ',last_name) AS name FROM `users` INNER JOIN user_profiles ON users.id = user_profiles.user_id WHERE users.id  NOT IN (SELECT user_id FROM mettings_attendance WHERE mettings_attendance.mettings_id = :id)"), [$id]);

        foreach ($metting->commitments as $item) {
            $users_name = DB::select(DB::raw("SELECT CONCAT(user_profiles.first_name,' ', user_profiles.last_name)name FROM `users` 
                              INNER JOIN user_profiles ON users.id = user_profiles.user_id INNER JOIN user_commitments ON user_commitments.user_id = users.id 
                              WHERE users.id and user_commitments.commitment_id = :id"), [$item->id]);
            array_push($users_acuerdos, $users_name);
        }

        $view = \View::make('pdf.invoice', compact('metting', 'minuta','attendances','users_acuerdos'))->render();

        //return $view;
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('letter','portrait');
        return $pdf->stream('invoice.pdf');
    }
}

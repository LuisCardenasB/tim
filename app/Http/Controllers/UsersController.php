<?php

namespace App\Http\Controllers;

use App\commitments;
use App\Http\Requests\UpdateUser as UpdateRequest;
use App\mettings_attendance;
use App\RoleUser;
use App\user_commitments;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\UserProfile;
use Session;
use Illuminate\Support\Facades\Redirect;
use HttpOz\Roles\Models\Role;
use App\Http\Requests\StoreUser as StoreRequest;
use Auth;
class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $role_user = RoleUser::where('user_id', Auth::user()->id)->first();
        $users = User::orderBy('created_at','desc')->paginate(30);
        return view('users.index')
            ->with('role_user', $role_user->role_id)
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles = Role::select(['id','description'])->get();
        $permission = 0;
        foreach($roles as $role) {
            $selectedRoles[$role->id] = $role->description;
        }
        return view('users.create')
            ->with("permission", $permission)
            ->with("roles",$selectedRoles);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        //
        $input = $request->all();
        $input["password"] = Hash::make($input["password"]);
        $input["name"] = $input["first_name"]." ".$input["last_name"];

        $user = new User();

        $user->fill($input);

        if ($user->save()){
            $input["user_id"] = $user->id;
            $profile = new UserProfile();
            $profile->fill($input);
            if ($request->hasFile('avatar')){
                $profile->avatar = $request->file('avatar')->store('avatars', 'public');
            }
            if ($request->hasFile('firm')){
                $profile->firm = $request->file('firm')->store('avatars', 'public');
            }
            $profile->save();
            $role = \HttpOz\Roles\Models\Role::whereSlug('user')->first();
            $user->attachRole($role);
            Session::flash('message', '¡Usuario creado!');
            return Redirect::to('users');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $permission = RoleUser::where('user_id', $id)->first();
        $user_profile = UserProfile::where('user_id', $id)->select('avatar', 'firm')->first();

        return view('users.update')
            ->with('permission', $permission->role_id)
            ->with('user_profile', $user_profile)
            ->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $input = $request->all();
        $input["password"] = Hash::make($input["password"]);
        $input["name"] = $input["first_name"]." ".$input["last_name"];

        $user = User::find($id);
        $user->fill($input);

        if ($user->save()){
            $input["user_id"] = $user->id;
            $profile = UserProfile::where("user_id",$id)->first();
            $profile->fill($input);
            if ($request->hasFile('avatar')){
                $profile->avatar = $request->file('avatar')->store('avatars', 'public');
            }
            if ($request->hasFile('firm')){
                $profile->firm = $request->file('firm')->store('avatars', 'public');
            }
            $profile->save();

            $admin = RoleUser::where('user_id', $id)->first();

            if ($request->remember == 'on')
            {
                $admin->role_id = 1;
            }
            elseif ($request->remember == null)
            {
                $admin->role_id = 2;
            }

            $admin->save();

            Session::flash('message', '¡Usuario actualizado!');
            return Redirect::to('users');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Obtenemos toda la informacion que se tiene en la tabla usuarios
        $user = User::find($id);

        // Buscamos en la tabla users_commitments para obtener todos los commitments que le corresponde
        // al usuario
        $user_commitments = user_commitments::where('user_id', $id)->get();

        foreach ($user_commitments as $user_com) {
            $id_commitments = $user_com->commitment_id;

            // Buscamos en la tabla commitments para poder obtener la informacion restante para los compromisos
            // y poder borrarlos
            $commitments = commitments::where('id', $id_commitments)->get();
            foreach ($commitments as $com) {
                $com->delete();
            }
            $user_com->delete();
        }

        $user_perfile = UserProfile::where('user_id', $id)->first();
        $user_perfile->delete();

        if($user->delete()):
            Session::flash('delete','El usuario ha sido eliminado');
            return Redirect::to('users');
        endif;

    }

    public function get_users(Request $request){
        $term = $request->term ?: '';

        $ids = mettings_attendance::where("mettings_id",$request->id)->select('user_id')->get();
        $tags = User::where('name', 'like', $term.'%')->whereNotIn("id",$ids)->pluck('name', 'id');
        $valid_tags = [];
        foreach ($tags as $id => $tag) {
            $valid_tags[] = ['id' => $id, 'text' => $tag];
        }
        return response()->json($valid_tags);
    }
}

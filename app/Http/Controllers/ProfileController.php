<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePerfil as UpdateRequest;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\UserProfile;
use Session;
use Hash;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = User::find(Auth::user()->id);
        return view('profile.edit')
            ->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        $user = UserProfile::where("user_id",$id)->first();
        if ($request->hasFile('avatar')){
            $user->avatar = $request->file('avatar')->store('avatars', 'public');
        }
        if ($request->hasFile('firm')){
            $user->firm = $request->file('firm')->store('avatars', 'public');
        }

        $user->fill($request->all());
        if($user->save()){
            $user = User::find(Auth::user()->id);
            if($request->has('password') && $request->password != null){
                $user->password = Hash::make($request->password);
                $user->save();
            }

            Session::flash('message', 'Perfil actualizado');
            return view('profile.edit')
                ->with('user', $user);
        }
    }

    public function update_image(Request $request)
    {

        $image_file = $request->image;

        // Obtiene el base64 y se va quitando a cadena de caracteres que no se va ocupando por ejemplo
        // en esta parte quitamos data:image/png y la guardamos en la variable $type y el resto lo dejamos
        // en $image_file
        list($type, $image_file) = explode(';', $image_file);

        // A continuación quitamos el base64; para que nos quede el resto y se guarda en $image_file
        list($base, $image_file)      = explode(',', $image_file);

        // Aqui separamos el tipo de imagen ya que puede ser de diferentes tipos las que suban.
        list($data,$type) = explode('/',$type);

        $image_file = base64_decode($image_file);

        $image_name = md5($image_file).'.'.$type;

//        $path = asset('/storage/avatars').'/'.$image_name;
        $path = public_path('/storage/avatars/'.$image_name);

        file_put_contents($path, $image_file);

        $user_perfil = UserProfile::where('user_id', $request->id)->first();
        $user_perfil->firm = 'avatars/'.$image_name;
        $user_perfil->save();

        return response()->json(['status'=>true]);
    }

    public function update_perfil(Request $request)
    {

        $image_file = $request->image;

        // Obtiene el base64 y se va quitando a cadena de caracteres que no se va ocupando por ejemplo
        // en esta parte quitamos data:image/png y la guardamos en la variable $type y el resto lo dejamos
        // en $image_file
        list($type, $image_file) = explode(';', $image_file);

        // A continuación quitamos el base64; para que nos quede el resto y se guarda en $image_file
        list($base, $image_file)      = explode(',', $image_file);

        // Aqui separamos el tipo de imagen ya que puede ser de diferentes tipos las que suban.
        list($data,$type) = explode('/',$type);

        $image_file = base64_decode($image_file);

        $image_name = md5($image_file).'.'.$type;

//        $path = asset('/storage/avatars').'/'.$image_name;
        $path = public_path('/storage/avatars/'.$image_name);

        file_put_contents($path, $image_file);

        $user_perfil = UserProfile::where('user_id', $request->id)->first();
        $user_perfil->avatar = 'avatars/'.$image_name;
        $user_perfil->save();

        return response()->json(['status'=>true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

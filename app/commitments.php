<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $metting_id
 * @property string $descriptions
 * @property string $end_date
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Metting $metting
 */
class commitments extends Model
{
	use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['metting_id', 'descriptions', 'end_date','tracking_date', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function metting()
    {
        return $this->belongsTo('App\mettings');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commitments_user()
    {
        return $this->belongsToMany('App\User','user_commitments','commitment_id','user_id');
    }
}

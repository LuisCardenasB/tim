<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $avatar
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 */
class UserProfile extends Model
{
	use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'first_name', 'last_name', 'positions', 'description', 'company', 'address', 'city', 'state', 'cp','initial','birthday','prefix','created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

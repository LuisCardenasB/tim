<?php

namespace App\Notifications;

use Illuminate\Auth\Notifications\ResetPassword ;
use Illuminate\Notifications\Messages\MailMessage;

class MyResetPassword extends ResetPassword
{
    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Recuperar Contraseña')
            ->greeting('Hola '.$notifiable->name)
            ->line('Estás recibiendo este correo porque se  hizo una solicitud de recuperación de contraseña para tu cuenta.')
            ->action('Recuperar contraseña', route('password.reset', $this->token))
            ->line('Si no realizaste esta solicitud, hacer caso omiso a este correo.')
            ->salutation('Saludos, ' . config('app.name'));
    }
}

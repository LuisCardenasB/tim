<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $date
 * @property string $title
 * @property string $start_date
 * @property string $agenda
 * @property string $description
 * @property string $minutes
 * @property string $conclusions
 * @property boolean $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property User $user
 * @property Commitment[] $commitments
 * @property MettingsAttendance[] $mettingsAttendances
 */
class mettings extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'date', 'title', 'start_date','location','owner', 'agenda', 'description', 'minutes', 'conclusions', 'status', 'created_at', 'updated_at', 'deleted_at'];

    protected $appends = array('status_commitmets');
    protected $dates = ['deleted_at'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commitments()
    {
        return $this->hasMany('App\commitments','metting_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mettingsAttendances()
    {
        return $this->hasMany('App\mettings_attendance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany('App\Attachment');
    }

    public function getStatusCommitmetsAttribute(){
        $commitmets = commitments::with(['commitments_user'])->where("metting_id",$this->id)->get(['id']);
        $id = [];
        foreach ($commitmets as $commitmet) {
            $id[] = $commitmet->id;
        }

        $statuss = user_commitments::whereIn("commitment_id",$id)->get();
        $masbajo[] = 4;
        $statues = ["Retrasado","Pendiente","Completo con retraso","Completo",""];
        foreach ($statuss as $status) {
            if($status->status_commitments == "Retrasado"){
                $masbajo[] = 0;
            }
            if($status->status_commitments == "Pendiente"){
                $masbajo[] = 1;
            }
            if($status->status_commitments == "Completo con retraso"){
                $masbajo[] = 2;
            }
            if($status->status_commitments == "Completo"){
                $masbajo[] = 3;
            }
            

        }
        return $statues[min($masbajo)];
    }
}

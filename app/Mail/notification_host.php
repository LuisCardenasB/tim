<?php

namespace App\Mail;

use App\commitments;
use App\mettings;
use App\user_commitments;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use DB;

class notification_host extends Mailable
{
    use Queueable, SerializesModels;

    protected $user_commitments;
    protected $commitments;
    protected $users;
    protected $metting;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id, $id_commitments)
    {
        $this->metting = mettings::find($id);

//        $this->user_commitments = user_commitments::join('commitments', 'user_commitments.commitment_id', '=', 'commitments.id')->get();
        $this->commitments   = user_commitments::select(['user_commitments.*','user_commitments.commitment_id','users.name','commitments.descriptions','commitments.end_date','user_commitments.progress'])
            ->where("metting_id",$id)
            ->where("user_commitments.status", "=", 1)
            ->where("user_commitments.commitment_id", $id_commitments)
            ->join('users', 'users.id', '=', 'user_commitments.user_id')
            ->join('commitments', 'commitments.id', '=', 'user_commitments.commitment_id')
            ->join("mettings","commitments.metting_id","=","mettings.id")->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'example@TIM.com', 'name' => 'TIM'])
            ->subject("Compromiso Completo")
            ->view('mail.notification_commitment')
            ->with("metting_title", $this->metting->title)
            ->with("metting_fecha", $this->metting->date)
            ->with("commitments", $this->commitments);
    }
}

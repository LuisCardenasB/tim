<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\mettings_attendance;
use App\commitments;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\mettings;
use DB;

class SendMinutes extends Mailable
{
    use Queueable, SerializesModels;

    protected $metting;
    protected $attendances;
    protected $commitments;
    protected $users;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        //
        $this->metting = mettings::find($id);
        $this->attendances   = mettings_attendance::where("mettings_id",$id)->join("users","users.id","=","mettings_attendance.user_id")->get();
        $this->commitments   = commitments::select(['users.name','commitments.descriptions','commitments.end_date'])
            ->where("metting_id",$id)
            ->join("user_commitments","user_commitments.commitment_id","=","commitments.id")
            ->join("users","user_commitments.user_id","=","users.id")
            ->get();
        $this->users  = DB::select(DB::raw("SELECT id, name FROM `users` WHERE id  NOT IN (SELECT user_id FROM mettings_attendance WHERE mettings_attendance.mettings_id = :id)"),[$id]);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'example@TIM.com', 'name' => 'TIM'])
            ->subject("Minuta de reunión")
            ->view('mail.sendminutes')
            ->with("metting",$this->metting)
            ->with("attendances",$this->attendances)
            ->with("commitments",$this->commitments)
            ->with("users",$this->users);
    }
}

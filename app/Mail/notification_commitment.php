<?php

namespace App\Mail;

use App\mettings;
use App\user_commitments;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class notification_commitment extends Mailable
{
    use Queueable, SerializesModels;

    protected $commitments;
    protected $metting;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id_user, $metting_id, $id_commit)
    {
        $this->metting = mettings::where('id',$metting_id)->orderby('created_at', 'DESC')->first();

        $this->commitments = user_commitments::select(['user_commitments.*','user_commitments.commitment_id','users.name','commitments.descriptions','commitments.end_date','user_commitments.progress'])
            ->join('users', 'users.id', '=', 'user_commitments.user_id')
            ->join('commitments', 'commitments.id', '=', 'user_commitments.commitment_id')
            ->join("mettings","commitments.metting_id","=","mettings.id")
            ->where('user_commitments.user_id', $id_user)
            ->where('user_commitments.status', 0)
            ->where('user_commitments.commitment_id', $id_commit)
            ->orderby('user_commitments.created_at', 'DESC')->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'example@TIM.com', 'name' => 'TIM'])
            ->subject("Compromiso Asignados")
            ->view('mail.notification_commitment')
            ->with("metting_title", $this->metting->title)
            ->with("metting_fecha", $this->metting->date)
            ->with("commitments", $this->commitments);
    }
}

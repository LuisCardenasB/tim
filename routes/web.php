<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to('mettings');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::post('avance', 'CommitmentsController@avance');
Route::post('commitments_edit', 'CommitmentsController@commitments_edit');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('mettings', 'MettingsController');
    Route::resource('commitments', 'CommitmentsController');
    Route::get('commitments/commitment', 'CommitmentsController@getData');
    Route::resource('attendace', 'AttendaceController');
    Route::put('attendace_metting/{id}', 'AttendaceController@get_by_metting');
    Route::put('add_attendace_metting/{id}', 'AttendaceController@add_attendance_metting');
    Route::patch('mettingss/{id}', 'MettingsController@close_meeting');
    Route::patch('mettings_save/{id}', 'MettingsController@update_meeting');

    Route::get('send_mail/{id}','MettingsController@sendMail');
    Route::post('upload/{id}','MettingsController@upload');
    Route::get('download/{id}','MettingsController@download');
    Route::get('print/{id}','MettingsController@metting_print');
    Route::resource('profile', 'ProfileController');


    Route::get('notifications/','NotificationsController@get_by_user');

// Generate PDF
    Route::get('pdf/{id}', 'PdfController@invoice');
// Update imagen de perfil y firma
    Route::post('profile/update_image', 'ProfileController@update_image');
    Route::post('profile/update_perfil', 'ProfileController@update_perfil');
});
Route::group(['middleware' => ['auth','role:admin']], function () {
    Route::resource('users','UsersController');
});

@role('admin')

<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <p>
            Administración
            <b class="caret"></b>
        </p>

    </a>
    <ul class="dropdown-menu">
        <li><a href="/users">Usuarios</a></li>
        <!--<li><a href="#">Another action</a></li>
        <li><a href="#">Something</a></li>
        <li><a href="#">Another action</a></li>
        <li><a href="#">Something</a></li>
        <li class="divider"></li>
        <li><a href="#">Separated link</a></li>-->
    </ul>
</li>
@endrole
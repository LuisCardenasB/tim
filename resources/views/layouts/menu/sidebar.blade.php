@if(Auth::user() != null)
<div class="sidebar-fixed position-fixed" style="background-image: url(images/pattern.png); background-position: right top;">
    <a class="logo-wrapper waves-effect">
        <img src="{{ asset('images/'.env('IMAGE_UPDATE_MENU').'') }}" class="img-fluid" alt="">
    </a>

    <div class="list-group list-group-flush">
        {{--            <a href="#" class="list-group-item  waves-effect">
                        <i class="fa fa-pie-chart mr-3"></i>Dashboard
                    </a>--}}
        <a href="{{ URL::to('profile') }}" class="item_sidebar list-group-item {{ Request::is('profile') ? 'active' : '' }} list-group-item-action waves-effect">
            <i class="pe-7s-user"></i> Profile</a>
        {{--            <a href="{{ URL::to('indicators') }}" class="list-group-item {{ Request::is('indicators') ? 'active' : '' }} list-group-item-action waves-effect">
                        <i class="fa fa-table mr-3"></i>Indicadores</a>--}}
        <a href="{{ URL::to('mettings') }}" style="border: none !important;" class="item_sidebar list-group-item {{ Request::is('mettings') ? 'active' : '' }} list-group-item-action waves-effect">
            <i class="pe-7s-note2"></i> {{trans('metting.mettings')}}</a>
        <a href="{{ URL::to('commitments') }}" style="border: none !important;" class="item_sidebar list-group-item {{ Request::is('commitments') ? 'active' : '' }} list-group-item-action waves-effect">
            <i class="pe-7s-bell"></i> {{trans('metting.commitments')}} <span class="badge badge-danger badge-pill" id="noNotifications">0</span></a>
        @role('admin')

        <a href="{{ URL::to('users') }}" style="border: none !important;" class="item_sidebar list-group-item {{ Request::is('users') ? 'active' : '' }} list-group-item-action waves-effect">
            <i class="pe-7s-user"></i> Usuarios</a>
        @endrole
        <a href="{{ url('/logout') }}" style="border: none !important;" class="item_sidebar list-group-item  list-group-item-action waves-effect"
           onclick="event.preventDefault();salir()">
            Salir
        </a>

        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>



</div>
@push('after_scripts')
<script>
    function salir(){
        var r = confirm("¿Estas seguro que desea salir?");
        if (r == true) {
            document.getElementById('logout-form').submit();
        }

    }
    $(function(){

    });
</script>
@endpush
@endif
@stack('before_scripts')
<!-- Scripts
    <script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>-->
<script src="{{ asset('js/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/functions.js') }}"></script>

<!--   Core JS Files   -->
<script src="{{ asset('assets/js/jquery-1.10.2.js') }}"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="{{ asset('assets/js/bootstrap-checkbox-radio-switch.js') }}"></script>

<!--  Charts Plugin -->
<script src="{{ asset('assets/js/chartist.min.js') }}"></script>

<!--  Notifications Plugin    -->
<script src="{{ asset('assets/js/bootstrap-notify.js') }}"></script>


<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="{{ asset('assets/js/light-bootstrap-dashboard.js') }}"></script>

<script src="{{ asset('js/filer/js/jquery.filer.min.js') }}"></script>
<script src="{{ asset('js/dropzone.js') }}"></script>
<script src="{{ asset('js/notify.min.js') }}"></script>
<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="{{ asset('assets/js/demo.js') }}"></script>
<script src="{{ asset('assets/bootstrap3-editable/js/bootstrap-editable.js') }}"></script>
<script src="{{ asset('js/moment.js') }}"></script>
<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>

<!-- JS croppie.js -->
<script src="{{asset('js/croppie.js')}}"></script>
<script src="{{asset('js/insertimage.js')}}"></script>
<script src="{{asset('js/insertimageperfil.js')}}"></script>

<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
@yield('scripts')
@yield('scripts2')
@yield('scripts3')
@stack('after_scripts')

<script>
    $( document ).ready(function(){
        @if(Auth::user())
            getNotificactions({{Auth::user()->id}});
        @endif


    })

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

</script>
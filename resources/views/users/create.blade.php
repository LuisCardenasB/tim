@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
        
            <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <div class="card">
                    <div class="header">
                        <h4 class="title">Crear usuario</h4>
                    </div>
                    <div class="content">
                        {!! Form::open(array('url' => '/users',  'id' => 'frmNewUser','enctype'=>'multipart/form-data')) !!}
                        {!! csrf_field() !!}
                        @include('users.field')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
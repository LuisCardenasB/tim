@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title">{{trans('user.users_index')}}</h4>
                        <a style="position: absolute; right: 40px;" href="./users/create" class="btn btn-success">{{trans('user.new_user_index')}}</a>
                        <p class="category">{{trans('user.list_of_users_index')}}</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        @if (Session::has('delete'))
                            <div class="alert alert-danger">
                                {{ Session::get('delete') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <table class="table table-hover">
                            <thead>
                            <th>{{trans('user.id_user_index')}}</th>
                            <th>{{trans('user.name_user_index')}}</th>
                            <th>{{trans('user.job_user_index')}}</th>
                            <th>{{trans('user.company_user_index')}}</th>
                            <th>{{trans('user.action')}}</th>
                            </thead>
                            <tbody>
                            @foreach($users as $key => $value)
                                <tr>
                                    <td>{{ $value->id}}</td>
                                    <td>{{ $value->profile->prefix }} {{ $value->profile->first_name }} {{ $value->profile->last_name }}</td>
                                    <td>
                                        {{ $value->profile->positions }}
                                    </td>
                                    <td>
                                        {{ $value->profile->company }}
                                    </td>
                                    <td>
                                        {!! Form::open(['route' => ['users.destroy' , $value->id], 'method'=>'DELETE']) !!}
                                        <a class="btn btn-sm btn-info" href="{{ URL::to('users/' . $value->id) }}/edit"><i class="fa fa-edit"></i>{{trans('user.edit')}}</a>
                                        @if($role_user == 1)
                                            <button type="submit" class="btn btn-small btn-danger" onclick="return confirm('¿Está seguro de eliminar al usuario seleccionado?')"><i class="fa fa-trash"></i></button>
                                        @endif
                                        {!! Form::close() !!}

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {!! $users->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="container">
        {!! $users->render() !!}
            </div>-->
@endsection
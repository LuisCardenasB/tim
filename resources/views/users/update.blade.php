@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                <div class="card">
                    <div class="header">
                        <h4 class="title">{{trans('user.user_update')}}</h4>
                    </div>
                    <div class="content">
                        {!! Form::model($user->profile,array('route' => array('users.update',  $user->id),'method'=>'PUT','id'=>'frmUpdate','enctype'=>'multipart/form-data')) !!}
                        {!! csrf_field() !!}
                            @include('users.field')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-user">
                    <div class="image" style="background-color: #9C1F00">
                        {{--<img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>--}}
                    </div>
                    <div class="content">
                        <div class="author">
                            <a href="#">
                                @if($user_profile->avatar == null)
                                    <img class="avatar border-gray" src="{{ asset('img/avatar_null.jpg') }}" alt="..."/>
                                @else
                                    <img class="avatar border-gray" src="{{ asset('storage/'.$user->profile->avatar) }}" alt="..."/>
                                @endif

                                <h4 class="title">{{$user->profile->first_name}} {{$user->profile->last_name}}<br />
                                    <small>{{$user->email}}</small>
                                </h4>
                                
                                @if($user_profile->firm != null)
                                    <img src="{{asset('storage/'.$user->profile->firm)}}" style="width: 50%">
                                @endif
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="text-center">
                        {{-- <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                         <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                         <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>--}}

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
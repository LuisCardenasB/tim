<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Nombre</label>
            {!! Form::text('first_name',null,["class"=>"form-control","placeholder"=>"Nombres"])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Apellidos</label>
            {!! Form::text('last_name',null,["class"=>"form-control","placeholder"=>"Apellidos"])!!}
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            <label>INICIALES</label>
            {!! Form::text('initial',null,["class"=>"form-control","placeholder"=>"Iniciales"])!!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group">
            <label>TITULO</label>
            {!! Form::text('prefix',null,["class"=>"form-control","placeholder"=>"Título"])!!}
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <label for="exampleInputEmail1">Correo electrónico</label>
            @if(isset($user->email))
                <input type="email" class="form-control" placeholder="Correo electrónico"  value="{{$user->email}}">
            @else
                {!! Form::email('email',null,["class"=>"form-control","placeholder"=>"Correo electrónico"])!!}
            @endif
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Contraseña</label>
            {!! Form::password('password',["class"=>"form-control","placeholder"=>""])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Confirmar contraseña</label>
            {!! Form::password('password_confirmation',["class"=>"form-control","placeholder"=>""])!!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Institución</label>
            {!! Form::text('company',null,["class"=>"form-control","placeholder"=>"Institución"])!!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Cargo</label>
            {!! Form::text('positions',null,["class"=>"form-control","placeholder"=>"Cargo"])!!}
        </div>
    </div>
</div>

{{--<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label>Dirección</label>
            {!! Form::text('address',null,["class"=>"form-control","placeholder"=>"Título"])!!}
        </div>
    </div>
</div>--}}

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Ciudad</label>
            {!! Form::text('city',null,["class"=>"form-control","placeholder"=>"Ciudad"])!!}
        </div>
    </div>
    @if($permission == 1)
        <div class="col-md-6">
            <div class="form-check" style="padding: 40px 0px 10px 50px;">
                <input type="checkbox" class="form-check-input" name="remember" style="padding: 40px 0px 10px 50px;" checked>
                <label class="form-check-label">{{trans('user.administrator_permission')}}</label>
            </div>
        </div>
    @else
        <div class="col-md-6">
            <div class="form-check" style="padding: 40px 0px 10px 50px;">
                <input type="checkbox" class="form-check-input" name="remember" style="padding: 40px 0px 10px 50px;">
                <label class="form-check-label">{{trans('user.administrator_permission')}}</label>
            </div>
        </div>
    @endif
  {{--<div class="col-md-6">
        <div class="form-group">
            <label>GRUPO</label>
            {{ Form::select('role[]', $roles, null, ['class' => 'form-control','']) }}
        </div>
    </div>
{{--    <div class="col-md-4">
        <div class="form-group">
            <label>Postal Code</label>
            {!! Form::text('cp',null,["class"=>"form-control","placeholder"=>"Título"])!!}
        </div>
    </div>--}}
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Imagen de perfil</label>
            {{ Form::file('avatar', ['class' => 'form-control-file','accept' => 'image/png,image/jpg']) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="">Firma Electronica</label>
            {{ Form::file('firm', ['class'=> 'form-control-file','accept' => 'image/png,image/jpg']) }}
        </div>
    </div>
</div>

<button type="submit" class="btn btn-info btn-fill pull-right">{{trans('metting.save')}}</button>
<div class="clearfix"></div>
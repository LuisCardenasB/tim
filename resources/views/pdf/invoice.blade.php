<!doctype html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="charset=utf-8"/>
    <title>Minuta de reunión</title>
    <link href="css/report.css" type="text/css">
</head>
    <body>
    <div class="contenedor">
        <div class="cabecera titulo">
            <p class="p_centrado">{{trans('metting.metting')}}</p>
            <p class="p_left_code">{{trans('metting.code')}}</p>
            <p class="p_left_revision">{{trans('metting.revision')}}</p>
            <p class="p_left_date">{{trans('metting.date_revision')}}</p>
        </div>

        <div class="logo">
            <!-- LOGO DE LA EMPRESA -->
            <img class="img_logo"  src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAASQAAABkCAMAAAAYCL0GAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAABvUExURQAAAHNzc3Nzc5sdAHNzc1AoIT4wLnNzc0kuKaEeAHFxcSIKBp0eAHV1dZkdAKQfAJcdAHZ2dnNzc6IfAHh4eFoRAJ4eAKogAKsgAKAeAHR0dF0RAHh4eKYfAJ4eAFgRAJ4eAGpqalYQAJ0eAKsgALFAtzwAAAAjdFJOUwBAwG+BGgf+EPQtKVfo7Zsw07OEm5Hgt9erVz1rQL1i0HJ8C8aPTQAADVRJREFUeNrtXAuXojoM5tUHCCLI4Migg+v8/994m5ZHgb7wjrvr2ck543CwhORr8iUtquc9Qw7Hg/fyQsP68jztaXe/h68O0SG/3+/HJylHx/39iep/UxDBPD/Ni8v1LuT8whD188zk+gTlYXe/P0/9b5L0Gj/Pi0M+Kb/f9y8aRGEne3H/VuU4nSv/ZvW/jawXTnynF/RYL7XfkdYSJ0G/HaH1PDMZe4BDuFXm7UN6jtfa7xc1mt0+dpP9NZXToN4rpE61Ll9V400UE9YKJyYv6niryLZdOpXy+11lf1qrx6olvuLJBc0QXc96UA7P9Wytsexo1GcUyTSkc1vRYqB4432mmT9rRtRYExfK0doO92LzIt2MUTxZhjvdIMWsnTffaYx27W00/djVrG4ptW2ajptNlww7OkTBGEjbQ3YMR/0QdXSox2rCzjB7VyPoJgntYcoSYZ33j4NkoAQlLanHdzpCMmjvh8SbTUf2MFW1GMfHQQoNY2pnStKsxIxU+ShvT1blblA+HLJjzOaO2W82LN3CX7MatT0JRkpOYyfSNfLE1yBGHVfH9Dfz/GFDIZxbsD0J0rHBcxpm4onTYTfIJe30TGuptgenErFX8vYhdpiA7XUZu1X00D5jezkl8Rr0zrGVWzLyxbHeGpsLmce+ZqIZKg85ORar3M4TuYXZc9e6mLuUCCVv2xJJILubidKcfSoP6Wcf7Z3bZS2vpBaDU70n9QzzOHWgYhVvf8QbvdDOWY631gRVDijMoZae7qBP7PN8kmaJq+lqFBsLuLOuLlyjLzXXhKtTo3Swdzi1jmljFeMdtFNycO6npnDch67tnoaMFaXzMM1jfnFqlFJrg4N1oY7UpTnV0VLo2k5JPUx6sC/mTYEaY1NNqBF2Up9bmfSyYQkONuFZ2McflqXYmrelcnrGeO/Y7qkbjM6YlRdtFU1tpXZx/8v7XE4HPQdwm+gMjZqaKeliSBugtOvdke5Tt00PqSYc9YuG0JhL60U53S3EEBnCJjRD/mTebVjlw+ccCRcv9NmcGmpCR/VtTG7h7c519znW2nTYKxIqddsCkJLthB292MDb+SKGDw4tRrhlM9XOAUgV+j0t5W7Onhb9A3VtlGqXOE3jxdxpOK+z8LbrxwVSUzkP17RUO6XNcRWWtVvnQF3ilNbzZNM2lvE23tbK0VTOcb48T51KuZSoJ2xci3ouC8Pckmz6BZAUgtjeb2vlbAwL9L544+LE29d1sx667Yu58Haq6DxS6+T9H97em4NwJ6fJ/kMzY502OkPz3sEq4HM7klJNmBoTalX/zbw9C8KLjOI77Rx4W+phTtjiReqwt7LXxv7F9kgmfBZvd4YEODm4KgG5323wQkccixIYqtt860JgA28jEDNvL4IQO+yuIk3GhNad8dzhQcxsjLQ/0VHrPvF5C2+jNyHnK8j57Q3pJ2CZATTftOOhTDYttV638vZJEwi251wO+yThfG/06wvpeXu1MzQvcRZPpcHxzu5FvZG3j7o1tXojYG+C/2wp9LWetxU7Qx97x0fB8zvN+UbTFNs3G2N1mHbUZTsdu+9vrwLmrM8B5ZZq7ProRlJ4wi67rcjWkMgG0Xf9Q5zc3Ch1Vl5Burk/Ou7oh468LfcwyOmpwMW6mZQrVeR0IalRvYq3D5ZCn+p5W1kXjSVur6TV88dCjvbuITWPkB8sXK+nuVyNLcbhbi1uuW7u967rGXp2eRIuB1y92OV7d2iUjsY4tRYQA12G9nVjp+FCN962WRi6Ery1CTob4zTfrn2cwNy+2ok1XHjZsJ75qM1P9OfJ5ixX22bS9X98uGhqlFTJmCNjtuX6INR/mlRb4ujjn/6ZRy4yJYzlwxEWvlTG+Ont7ddnL/l9A2+jTVtPspuz57VfCrHuKF1MvC3ZGp9yhZxMjZL63syo4eO8eiRq24rbvms4pj2WSOv9+Eshua1RCg3vy++FiCpktzc0So/HIHJrJS2flQxXcVZ/qEG+WholA2/LCOi+3WRS/zjlXxxbSXMkHVa6dLxmW74aeFvKpSveQgfhoyCleqMv2/aeRt6Wd+O0QF/MjRLV87Zkab3bZN/xwfbhhB/ibc2N6uV72pm2rToO2gndxS7lFxmyZmsjet3pl5P1xu3wwYrUZaYtfZCet3Hnxgem5fFH/h5/OUr8nk9OfK2+12LaGqdf2q/CyD1Muu3RjLQvdnxfy2lBNh02qL8a+7CPz19vbvIpF57PlexMzww+lULnzhu/+RoaG6UPhezmVLbfbXyG+Pd8NTCUn6eYBh5UwW3TLtcE8/ON3fm0kvwvwUjuYT7M/qqCe8MzZtsXtPG6xfxLMMInN1p9UKSa8I68V5Vw/SGmbxSpJsQfL4uR1MPUT5jpfPsD179P5L359KnJdnpZjGZ780+tCfXrEpLUw1y/v5LINSF93WSbaHX/hN8rCp8apn8g2cKnJtvr/jqLTKtP+CEeenpqmP4mkXqYd/rUZHvd6i997HT/hD7v8tQw/QOt9hNmmnbjbsyJvixGtKt7eX/GTKO34SnL5+ti5KF0kNddU/3Ij/zIj/zIj/zIvysY/bGtKulBBd3tdP0gkr+RhqVfatM/Kze2lhh7DjrmhjZV5W9sEW3tLRUjsG1cE8FgsBO3ZemLw5VUiXRF2wxTitpWY3dStSbj2HXDfZK2TVw8bkmWlWrEvUdsYNb7UdXu2NV+Zbl3FAAG4GqT+b7vqWeLSGcJIYPSkpBCo5aUhpv6TEfT45URErmAlBHqKfMtaekjNni4zG63MmMzdMtcQMpu4DF3nR9aQCoyMXmUFCTQzFKTGEEqBgcadugEktbjiPiP2MCMAHATAJ66gMQHBdxW9fg5SG0fBjd2FAhO3e2AtkSOY3gVaYuGXwiC3zuYiM0nQUnEncos4iBxFbRnEjRchbxeH0IkQEwBHYeJQ4xRRBqgdHGWcjNAAxI2YBgykC1cMX5HLgqmkuD1F8G4YTSdLhQgUaY9iPi37OjokzcqXoBExazirPA5SLgpWHixqG8K8NcvGi8pgA8Slo9ZyeYTVSyrsjEv2GUNuYlgjDhIuC0yknHqK1qfXQZXeRVPZtBVwR2KhEbsfxYB2qCSGZDAdQUbV/BEaAt2XVUlEKnchqTwW3ZNBbcGvUVR9Axxy4bsTeBMWyUVs4CKV7h525T9oeCk1mtKxotMuHOI2cSdA8UZs30OkifCwCeNAKklpc90Rx4uSOLRIqPsPeY5ykh5i7IIiKe83aa8YpfhjOdsRBAHKSHF7RZwsiMBG1xysguIGB157AQzD7UZ19MCo5CAHQaoZClflnDZmHtBkRXsFLfBJyWJhGaaZT6HYGDDso9tH24TFQGb26wqbsxuGBJkjK9wUA0gsUyjSVElTCAWcBmwtyNAGsGNmFlzkBrubkkwBwkTTnwFwUA2NIJk5AZW3GmUMBT5AelTDEBib/oQjIHHQUK+IGeu/8a1URmkHgTOIAkEciNKhg+48NmUQRLHPUhwhsLoFsD1irHU+FkWcZgESATx+QYmaTMOkrhZMoE0cBK8NoLtAa+qFOQzB4klGqMH5jkHqSEV5Ck3lsUTz0VuYEF65uwPqqGkwWUJANewM1EfYJhSDorwvvd2ARIfheGwIu2cuGcgIQkk4RYZBgqdIpairGjwAFIwBpV4DUQJy1o1SFXFPw4DABWtgrg9HgYt++MgRUAKBeMGnwcDB4TbRoh0CfcgmkDikceCsT9LWdNSiUgKDCAh1lJWcBiMJqlA8lQgNaTFmM6aFlyJsFeCJIyFfyqQgiACCcoRnSVIEPEQtz1IjChAoJgwGqVuILHoh2AUZymQYGkFyc8IY5sHQWKcxLg4a+ZtcvEoSL3TvhYkSCBhAvd26lgh3QI8gkSnRnCZbixdAwhGAVLEdQQ2kAquAQ7LQZc7SC2JGMbNsluygFRo0q2curvelBVIN8Id594mInhgDQYRVkrE3ULXkgwHmUzc4GdWDPHFAwMvQSohdWk5gcTvTcXMlBC4yQgSr5aoMIAUFFhexQhbgH81IMEN9MTdFmPbJ9pwfwUSJT0/C2/LhCZtBS0A67jI0AKwytJQqJfioBwjTlzWiDoWCTwjSluyAIn1qknCklCOJMaVPA7ZWdbT8LpRUp+XUpoIYtSAdONNUtXgYT0K7SKr+DqQ2A0whe5CCRJmbR+juCQBoFnH6mcrkPpw9zlUDBuQiDl141EW9bZFcBpWRw0fUOIRJH6ZiCzBSRl4EJFx/cFB4oqLRIAkJoUEWcUNbzIiVoAYDtiUw7/SFwG4AqkUnMT6wHGpyKg0qrJybAHKVboVQRRAcyre4/klkoy/JhmjbkFxVRZVxSySEl7OqajuiQja5HZrKG+J+hGYdzRwXiz3aXu7TSspLC4TmnDC9yCaWyOuF2cRP+uxs7jvjpJejz8dNmKno+WqMbzFlYn3hQ2DIQmERiKmZtx8idreAm+wHU+vLFyaSISdsAsNL/0r9qOo38zwI8Yniduez98t/TqzyNyGB5H3DwpbzjCSiUj0A5JekoBTpKvv/yZIwCJ+4o4p8n7kGfIf118dhfzv3nEAAAAASUVORK5CYII=">
        </div>

        <div class="asunto">
            <table>
                <tr>
                    <th class="titulo" style="width: 75px">
                        <span class="bolds">{{trans('metting.affair_PDF')}}</span>
                    </th>
                    <th style="width: 381px">
                        {{$metting->title}}
                    </th>
                    <th class="titulo" style="width: 75px">
                        <span class="bold">{{trans('metting.date_PDF')}}</span>
                    </th>
                    <th style="width: 100px">{{\Carbon\Carbon::parse($metting->date)->format('d/m/Y')}}</th>
                </tr>
            </table><p></p><br>
        </div>

        <div class="participantes">
            <table class="tamano_width_100">
                <tr>
                    <th colspan="3" class="center titulo tamano_height_20">
                        <strong>{{trans('metting.participants_PDF')}}</strong>
                    </th>
                </tr>
                <tr>
                    <td class="tamano_width_50 bold center"> {{trans('metting.name_PDF')}} </td>
                    <td class="tamano_width_50 bold center"> {{trans('metting.job_PDF')}} </td>
                    <td class="tamano_width_50 bold center"> {{trans('metting.firm_PDF')}} </td>
                </tr>
                @foreach($attendances as $key => $value)
                    <tr>
                        @if($value->status == 1)
                            <td>{{$value->prefix}} {{$value->first_name}} {{$value->last_name}}</td>
                            <td>{{$value->positions}}</td>
                            @if($value->firm)
                                <?php
                                    $imagedata = file_get_contents(public_path('/storage/'). $value->firm);
                                    $type = pathinfo(public_path('/storage/'). $value->firm, PATHINFO_EXTENSION);

                                    // alternatively specify an URL, if PHP settings allow
                                    $base64 = base64_encode($imagedata);
                                ?>
                                    <td><img src="data:image/{{$type}};base64,{{$base64}}" class="img_firm"></td>
                            @else
                                <td></td>
                            @endif
                        @else
                            <td></td>
                            <td></td>
                            <td></td>
                        @endif
                    </tr>
                @endforeach
            </table><p></p>
        </div>

        <div class="comentarios">
            <table class="tamano_width_100">
                <tr>
                    <th class="center titulo tamano_height_20">
                        <strong>{{trans('metting.development_comments_PDF')}}</strong>
                    </th>
                </tr>
                <tr><td class="tamano_width_50">{{$metting->agenda}}</td></tr>
                <tr><th class="tamano_width_50">{{trans('metting.metting_PDF')}}</th></tr>
                <tr>
                    <td class="tamano_width_50 font-size">
                    @foreach($minuta as $value)
                            {{$value}}<br>
                    @endforeach
                    </td>
                </tr>
            </table><p></p>
        </div>

        <div class="acuerdos">
            <!-- ACUERDOS -->
            <table class="tamano_width_100">
                <tr>
                    <th colspan="4" class="center titulo tamano_height_20">
                        <strong>{{trans('metting.agreements_PDF')}}</strong>
                    </th>
                </tr>
                <tr>
                    <td class="bold tamano_width_50 center">{{trans('metting.activity_PDF')}}</td>
                    <td class="bold tamano_width_50 center">{{trans('metting.responsible_PDF')}}</td>
                    <td class="bold tamano_width_50 center">{{trans('metting.commitment_date_PDF')}}</td>
                    <td class="bold tamano_width_50 center">{{trans('metting.tracking_date_PDF')}}</td>
                </tr>
                @foreach($metting->commitments as $key => $item)
                    <tr>
                        <td>{{$item->descriptions}}</td>
                        <td>
                            <p>

                                @foreach($users_acuerdos[$key] as $key => $value)
                                    {{$value->name}}<br>
                                @endforeach
                            </p>
                        </td>
                        <td class="center">{{\Carbon\Carbon::parse($item->end_date)->format('d-m-Y')}}</td>
                        <td class="center">{{\Carbon\Carbon::parse($item->tracking_date)->format('d-m-Y')}}</td>
                    </tr>
                @endforeach
            </table><p></p>
        </div>

        <div class="documentos">
            <!-- DOCUMENTOS ENTREADOS -->
            <table class="tamano_width_100">
                <tr>
                    <th class="titulo center tamano_height_20">
                        <strong>{{trans('metting.documents_delivered_PDF')}}</strong>
                    </th>
                </tr>
                @for($i = 0; $i < 3; $i++)
                    <tr><td>&nbsp;</td></tr>
                @endfor
            </table><p></p>
        </div>

        <div class="observaciones">
            <!-- OBSERVACIONES -->
            <table class="tamano_width_100">
                <tr>
                    <th class="bold titulo center">
                        <strong>{{trans('metting.observations_PDF')}}</strong>
                    </th>
                </tr>
                @for($i = 0; $i < 1; $i++)
                    <tr>
                        <td></td>
                    </tr>
                @endfor
            </table>
        </div>
    </div>

    </body>
</html>
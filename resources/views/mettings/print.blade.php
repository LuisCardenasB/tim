<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TIM') }}</title>

<!-- Styles
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="{{ asset('css/loader.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>


    <!-- Styles -->
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- Animation library for notifications   -->


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('assets/css/print.css') }}" rel="stylesheet" media="print" >


</head>
<body>



<div class="wrapper">

    <div class="content">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12" >
                        <img src="http://tim.cmicsonora.org/images/logoici.png" id="logo" width="30%" style="background-color: red; margin: 20px;">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">

                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <strong>Comité Directivo</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Fecha:</strong> {{\Carbon\Carbon::parse($metting->date)->format('d/m/Y')}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <strong>Reunión:</strong> {{$metting->title}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">

                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3><strong>MINUTA UNIVERSAL</strong></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <strong>Hora:</strong> {{\Carbon\Carbon::parse($metting->date)->format('h:i A')}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
                                    \Carbon\Carbon::setLocale('es');
                                    ?>
                                    <strong>Duración: </strong> {{\Carbon\Carbon::parse($metting->updated_at)->diffForHumans(\Carbon\Carbon::parse($metting->date))}}
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    <strong>Lugar:</strong> {{$metting->location}}
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td><strong>Participantes (Nombre, cargo, institución e iniciales):</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <ul>
                                @foreach($attendances as $key => $value)
                                    @if($value->status == 1)
                                        <li> {{$value->prefix}} {{$value->first_name}} {{$value->last_name}}, {{$value->positions}}, {{$value->company}}, {{$value->initial}}</li>
                                    @else
                                       <!-- <li><i class="glyphicon glyphicon-remove" style="color: red;"></i> {{$value->prefix}} {{$value->first_name}} {{$value->last_name}}</li>-->
                                    @endif
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td><strong>Orden del día:</strong></td>
                    </tr>
                    <tr>
                        <td>
                            {!! $metting->agenda !!}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td><strong>Resumen:</strong></td>
                    </tr>
                    <tr>
                        <td>
                            {!! $metting->conclusions!!}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td><strong>Temas:</strong></td>
                    </tr>
                    <tr>
                        <td>
                            {!! $metting->minutes!!}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td><strong>Acuerdos:</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <table class="table table-bordered" class="table class="table table-bordered"">
                                <thead>
                                <tr >
                                    <th class="text-center">#</th>
                                    <th class="text-center">Resp </th>
                                    <th class="text-center">Descripción </th>
                                    <th class="text-center">Fecha </th>
                                    <th class="text-center">Estado</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($commitments as $key => $value)
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->descriptions}}</td>
                                        <td>{{\Carbon\Carbon::parse($value->end_date)->format('d-M-y')}}</td>
                                        <td class="text-center">
                                            @if($value->status == 1)
                                                <i class="glyphicon glyphicon-ok" style="color: #0d904f"></i>
                                            @else
                                                {{--<i class="glyphicon glyphicon-remove" style="color: red;"></i>--}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td><strong>Relación de anexos:</strong></td>
                    </tr>
                    <tr>
                        <td>
                            @if(count($metting->attachments) == 0)
                                Sin Anexos
                            @endif
                            <ul>
                                @foreach($metting->attachments as $key => $value)
                                    <li>{{$value->name}}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tr>
                        <td  rowspan="2"  width="100" style="vertical-align: middle;"><div style="transform: rotate(270deg);text-align: center;vertical-align: middle;"><strong>Firmas de Validación</strong></div></td>
                        <td class="text-center" height="150">Responsable</td>
                    </tr>
                    <tr>
                        <td class="text-center" height="50">
                            {{--{{Auth::user()->profile->first_name}} {{Auth::user()->profile->last_name}} <br>
                            {{Auth::user()->profile->positions}}--}}
                            {{$metting->owner}}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    javascript:window.print()
</script>
</body>
</html>


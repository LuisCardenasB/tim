<!--Modal: modalPush-->
<div class="modal fade" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
    <div class="modal-dialog modal-notify modal-info" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">{{ trans('metting.Do_you_want_to_add_a_new_commitment?') }}</p>
            </div>
            <!--Body-->
            <div class="modal-body">
                
            </div>

            <!--Footer-->
            <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-outline-success btn-rounded btn-md ml-4" data-dismiss="modal" data-toggle="modal" data-target="#compromisoModal">{{ trans('metting.yes') }}</button>
                <button type="button" class="btn btn-outline-danger btn-rounded btn-md ml-4" data-dismiss="modal">{{ trans('metting.no') }}</button>
            </div>
    
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: modalPush-->






{{-- <div class="modal fade" id="modalconfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-notify modal-success"  role="document">
        <!--Content-->
        <div class="modal-content">
        <!--Header-->


        <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    {{ trans('metting.Do_you_want_to_add_a_new_commitment?') }}
                </div>
            </div>

            <!--Footer-->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">NO</button>
                <button type="submit" class="btn btn-primary" data-dismiss="modal" data-toggle="modal" data-target="#compromisoModal">SI</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div> --}}
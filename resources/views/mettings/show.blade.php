@extends('layouts.app')

@section('content')
    <section class="mb-4">

        <div class="">
            <div class="card-body d-flex justify-content-between">
                <h3 class="card-title h3-responsive mt-3">{{$metting->title}}</h3>
                <div>
                    {{--@if($metting->status == 3)--}}
                        {{--<a href="{{ URL::to('send_mail/'. $metting->id ) }}" class="btn btn-success waves-effect waves-light"> <i class="glyphicon glyphicon-send"></i> {{trans('metting.send_minutes')}}</a>--}}
                        {{--<a href="{{ URL::to('pdf/'. $metting->id ) }}" target="_blank" class="btn btn-primary waves-effect waves-light"> <i class="glyphicon glyphicon-print"></i> {{trans('metting.to_print')}}</a>--}}
                    {{--@else--}}
                        <button class="btn btn-dark-green" type="button" data-toggle="modal" data-target="#addAttendaceModal"> <i class="glyphicon glyphicon-user"></i> {{trans('metting.add_guests')}}</button>
                        <a href="{{ URL::to('mettings/' . $metting->id . '/edit') }}" class="btn btn-primary" > <i class="glyphicon glyphicon-record"></i> {{trans('metting.btn_start_table_list')}}</a>
                    {{--@endif--}}
                </div>

            </div>
        </div>
    </section>
    <section>
        <div class="row">
            <!-- Reunion -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        {{ trans('metting.development_of_the_meeting_fields') }}
                    </div>
                    <div class="card-body">
                        <!--Grid row-->
                        <div class="row">
                            <!--Grid column-->
                            <div class="col-md-12 text-justify">
                                <h4>{{ trans('metting.order_of_the_day_fields') }}</h4>
                                {!! $metting->agenda !!}
                            </div>
                            <div class="col-md-12 text-justify">
                                <h4>{{ trans('metting.metting_lower') }}</h4>
                                {!! $metting->minutes !!}
                            </div>
                            <div class="col-md-12 text-justify">
                                <h4>{{ trans('metting.conclusion') }}</h4>
                                {!! $metting->conclusions !!}
                            </div>
                            <!--Grid column-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Asistentes -->
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        {{ trans('metting.list_of_assistants_add_attendance') }}
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <!-- Item list -->
                            <table class="table">
                                <thead>
                                <tr></tr>
                                </thead>
                                <tbody>
                                @foreach($attendances as $key => $value)
                                    <tr>
                                        @if($value->status == 1)
                                            <td><i class="pe-7s-check" style="color: #0d904f; font-size: 22px; font-weight: bold;"></i></td>
                                            <td>{{$value->prefix}} {{$value->first_name}} {{$value->last_name}}</td>
                                        @else
                                            <td><i class="pe-7s-close-circle" style="color: red; font-size: 22px; font-weight: bold;"></i></td>
                                            <td>{{$value->prefix}} {{$value->first_name}} {{$value->last_name}}</td>
                                        @endif
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                            <!-- /.Item list -->
                        </div>

                    </div>
                </div>
            </div>
            <!-- Compromisos -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        {{ trans('metting.list_commitments') }}
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <!-- Item list -->
                            <table class="table">
                                <thead>
                                <tr >
                                    <th class="text-center">{{ trans('metting.user') }} </th>
                                    <th class="text-center">{{ trans('metting.description') }} </th>
                                    <th class="text-center">{{ trans('metting.advance') }} </th>
                                    <th class="text-center">{{ trans('metting.expiration_date') }} </th>
                                    <th class="text-center">{{ trans('metting.status') }}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($commitments as $key => $value)
                                    <tr>
                                        <td style="width:80px" class="text-justify">{!!$value->user->name!!}</td>
                                        <td style="width:80px" class="text-justify">{!!$value->commitment->descriptions!!}</td>
                                        <td style="width:60px" class="text-justify">{!!$value->progress!!}</td>
                                        <td style="width:80px" class="text-center">{!!\Carbon\Carbon::parse($value->commitment->end_date)->format('d-m-Y')!!}</td>
                                        <td class="text-center" style="width:60px">
                                            @if($value->status == 1)
                                                <i class="fa fa-check-circle" style="color: #0d904f"></i> {{$value->status_commitments}}
                                            @else
                                                <i class="fa fa-times-circle" style="color: red;"></i> {{$value->status_commitments}}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- /.Item list -->
                        </div>



                    </div>
                </div>
            </div>
            <!-- Adjuntos -->
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        {{ trans('metting.attached') }}
                    </div>
                    <div class="card-body">
                        <!--Grid row-->
                        <div class="row">
                            <!--Grid column-->
                            <div class="col-md-12 ">
                                <ul>
                                    @foreach($metting->attachments as $key => $value)
                                        <li><a href="{{ URL::to('download/'. $value->id ) }}"> <i class="glyphicon glyphicon-document"></i> {{$value->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <!--Grid column-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mb-4">

    </section>
   
    @include('mettings.add_attendance')
@endsection

@section('scripts')
    <script>
       // $(".navbar").css("", "");

    </script>

@endsection
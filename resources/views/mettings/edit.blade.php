@extends('layouts.app')
@section('content')
        <div class="row">
            <div class="col-md-12 col-md-offset-0">
                {{--@if(isset($metting->id))--}}
                    {{--Reunión--}}
                {{--@else--}}
                    {{--Crear nueva reunión--}}
                {{--@endif--}}
                    {!! Form::model($metting,array('route' => array('mettings.update',  $metting->id),'method'=>'PUT','id'=>'frmUpdate')) !!}
                    {!! csrf_field() !!}
                    @include('mettings.fields')
                    {!! Form::close() !!}
                @if(env('METTING_UPLOAD_FILE') != false)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Adjuntos</h3> Archivos
                        </div>
                        <div class="panel-body">
                            <form action="/upload/{!! $metting->id !!}" class="dropzone" id="my-dropzone">
                                {!! csrf_field() !!}
                            </form>

                        </div>
                    </div>
                @endif
            </div>
        </div>
    @include('mettings.modal.compromiso')
    @include('mettings.modal.attendance')
    @include('mettings.modalconfirm')
    @include('mettings.modal.commitments_list')
    @include('mettings.modal.modal_upload_file')

    <!-- /.content -->
@endsection

@section('scripts2')
    <script>
        function save(){
            var obj = {
                // "agenda":CKEDITOR.instances["agenda"].getData(),
                "minutes":CKEDITOR.instances["minutes"].getData(),
                "owner":$("#owner").val()
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ url('/mettings_save/'.$metting->id) }}',
                type: 'PATCH',
                data: obj,
                success: function(data) {
                    if(data.status){
                        $.notify("Se ha guardado.");
                    }
                }
            });
        }
        $( document ).ready(function() {
                $.notify.defaults({ className: "success", position:"rigth bottom" });
                $( "#frmUpdate" ).submit(function( event ) {
                    event.preventDefault();
                    if (confirm('¿Estas seguro que deseas cerrar la reunión?')) {
                        $('#loader-wrapper').show();
                        var obj = {
                            // "agenda":CKEDITOR.instances["agenda"].getData(),
                            "minutes":CKEDITOR.instances["minutes"].getData()
                        }
                        $.ajax({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            url: '{{ url('/mettingss/'.$metting->id) }}',
                            type: 'PATCH',
                            data: obj,
                            success: function(data) {
                                if(data.status){
                                    window.location = '{{ url('/mettings') }}'
                                }
                            },
                            beforeSend:function() {

                            },
                            complete: function(){
                                setInterval(function(){
                                    $('#loader-wrapper').hide();
                                },2000)
                            }
                        });
                    }
                });

                $("#attendaceModal").modal("show");

            setInterval(function(){
                save();
            },30000)
        });
        Dropzone.options.myDropzone = {
            init: function() {
                this.on("addedfile", function(file) {

//                    // Create the remove button
//                    var removeButton = Dropzone.createElement("<button >Remove file</button>");
//
//
//                    // Capture the Dropzone instance as closure.
//                    var _this = this;
//
//                    // Listen to the click event
//                    removeButton.addEventListener("click", function(e) {
//                        // Make sure the button click doesn't submit the form:
//                        e.preventDefault();
//                        e.stopPropagation();
//
//                        // Remove the file preview.
//                        _this.removeFile(file);
//                        // If you want to the delete the file on the server as well,
//                        // you can do the AJAX request here.
//                    });
//
//                    // Add the button to the file preview element.
//                    file.previewElement.appendChild(removeButton);
                });
            },
            addRemoveLinks: true,
            dictRemoveFile: 'Remover documento',
            maxFilesize: 16, // MB
            acceptedFiles: 'image/*,application/pdf,.doc,.docx,.xls,.xlsx,.txt'
        }
    </script>

@endsection
@extends('layouts.app')

@section('content')

          <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header card-body">
                                <h3 class="card-title">{{trans('metting.meeting_bitacora_list')}}</h3>
                                <a href="./mettings/create" class="btn btn-dark-green add_btn">{{trans('metting.new_meeting_list')}}</a>

                                <p class="category">{{trans('metting.list_of_minutes_list')}}</p>
                                @if (Session::has('message'))
                                    <div class="alert alert-info">
                                        {{ Session::get('message') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                                @if (Session::has('delete'))
                                    <div class="alert alert-danger">
                                        {{ Session::get('delete') }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @endif
                            </div>
                            <div class="content table-responsive ">
                                <table class="table table-hover">
                                    <thead>
                                        {{--<th>{{trans('metting.id_table_list')}}</th>--}}
                                    	<th>{{trans('metting.title_table_list')}}</th>
                                    	<th>{{trans('metting.date_table_list')}}</th>
                                    	<th>{{trans('metting.responsible_table_list')}}</th>
                                        <th>{{trans('metting.state_table_list')}}</th>
                                    	<th></th>
                                    </thead>
                                    <tbody>
                                    @foreach($meetings as $key => $value)
                                        <tr class='clickable-row' href="{{ URL::to('mettings/' . $value->id) }}">
                                            <td>{{ $value->title }}</td>
                                            <td>{{ \Carbon\Carbon::parse($value->date)->format('Y-m-d H:i a')}}</td>
                                            <td>
                                                {{$value->owner}}

                                            </td>
                                            <td>
                                                {{$value->status_commitmets}}
                                            </td>
                                            <td>
                                                {!! Form::open(['route' => ['mettings.destroy' , $value->id], 'method'=>'DELETE']) !!}
                                                @if($value->status == 3)
                                                    <a class="btn btn-small btn-grey" href="{{ URL::to('pdf/'. $value->id ) }}" target="_blank" data-toggle="tooltip" data-placement="top" title="Ver minuta"><i class="fa fa-file"></i></a>
                                                    <a class="btn btn-small btn-indigo" href="{{ URL::to('send_mail/' . $value->id) }}" data-toggle="tooltip" data-placement="top" title="Enviar minuta"><i class="fa fa-envelope"></i></a>
                                                @endif
                                                @if($value->status < 3)
                                                    <a class="btn btn-small" href="{{ URL::to('mettings/' . $value->id) }}" data-toggle="tooltip" data-placement="top" title="Detalle de la reunión"><i class="fa fa-external-link"></i></a>
                                                    <a class="btn btn-small btn-primary" href="{{ URL::to('mettings/' . $value->id . '/edit') }}" data-toggle="tooltip" data-placement="top" title="Iniciar reunión"><i class="fa fa-play"></i></a>
                                                @endif
                                                @if($role_user == 1)
                                                    <button type="submit" class="btn btn-small btn-danger" onclick="return confirm('¿Está seguro que desea eliminar la minuta especificada?')" data-toggle="tooltip" data-placement="top" title="Borrar Reunión"><i class="fa fa-trash"></i></button>
                                                @endif
                                                {!! Form::close() !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                 </tbody>
                                </table>
                                {!! $meetings->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection


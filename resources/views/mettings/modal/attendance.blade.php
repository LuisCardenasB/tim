@if(isset($users))
    <!-- Modal -->
    <div class="modal fade" id="attendaceModal" tabindex="-1" role="dialog" aria-labelledby="attendaceModal" aria-hidden="true"> --}}
        <div class="modal-dialog modal-notify modal-info" role="document">
            <div class="modal-content">
                <div class="modal-header d-flex justify-content-center">
                    <p class="heading">{{ trans('metting.commitments') }}</p>
                </div>
                {{-- <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{trans('metting.list_attendance')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> --}}
                {{-- <form id="frmHola">

                </form> --}}
                <form id="frmAttendance">
                    <div class="modal-body">
                        {!! csrf_field() !!}
                        @foreach($attendence as $user)
                            <?php
                            $checked = '';
                            if($user->status){
                                $checked = "checked";
                            }
                            ?>
                            <div class="checkbox {{$checked}}">
                                <span class="icons"><span class="first-icon fa fa-square-o"></span><span class="second-icon fa fa-check-square-o"></span></span>
                                <label>

                                    <input type="checkbox" name="attendaces[]" value="{{$user->user_id}}" {{$checked}}>{{$user->prefix}} {{$user->first_name}} {{$user->last_name}}</label>
                            </div>
                        @endforeach
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-outline-success btn-rounded btn-md ml-4" type="button" id="btnAttendence">{{trans('metting.save')}} </button>
                        <button type="button" class="btn btn-outline-danger btn-rounded btn-md ml-4" data-dismiss="modal">{{ trans('metting.close_fields') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@section('scripts3')
    <script>
        $( document ).ready(function() {
            $( "#btnAttendence" ).on('click',function() {
                $.ajax({
                    url: '{{ url('/attendace_metting/'.$metting->id) }}',
                    type: 'PUT',
                    data: $("#frmAttendance").serialize(),
                    success: function(data) {
                        console.log(data);
                        if(data.status){
                            $('#attendaceModal').modal('hide');
                        }

                    }
                });
            });
        });

    </script>
@endsection

@endif


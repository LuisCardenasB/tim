@if(isset($users))
    <!-- Central Modal Large Info-->
    <div class="modal fade" id="compromisoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-notify modal-info"  role="document">
            <!--Content-->
            <div class="modal-content">
            {!!Form::hidden('id',null,['id'=>'id'])!!}
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">{{ trans('metting.commitments') }}</p>
            </div>
                {{-- <div class="modal-header" style="background-color: #fafafa !important;">
                    <p class="heading lead" style="color: #867e7e !important">{{trans('metting.commitments')}}</p> --}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true" class="white-text">&times;</span>--}}
                    {{--</button>--}}
                {{-- </div> --}}

            {!! Form::open(array('url' => '/commitments', 'id' => 'frmNewCommitments')) !!}
            <!--Body-->
                <div class="modal-body">
                    {{--<div class="text-center">--}}
                        {{--<i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>--}}
                    {{--</div>--}}
                    <div class="row">
                    <div class="col-md-6">
                            <div class="form-group">
                                <label fobr="message-text" class="control-label">{{trans('metting.description')}}:</label>
                                {{Form::textarea('descriptions',null,array("class"=>"form-control",'id'=>'descriptions'))}}
                            </div>
                            <div class="form-group">
                                <label for="message-text" class="control-label">{{trans('metting.expiration_date')}}:</label>
                                {{Form::date('end_date',\Carbon\Carbon::now()->addWeek(),array("class"=>"form-control",'id'=>'mdlend_date'))}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">{{trans('metting.responsible')}}:</label>
                                {{ Form::select('users[]', $users, null, ['multiple' => true, 'class' => 'form-control','id'=>"commitmets"]) }}
                            </div>
                            @if(env('METTING_TRACKING_DATE') != false)
                                <div class="form-group">
                                    <label for="message-text" class="control-label">{{trans('metting.tracking_date')}}:</label>
                                    {{Form::date('tracking_date',\Carbon\Carbon::now()->addWeek(2),array("class"=>"form-control",'id'=>'mdltracking_date'))}}
                                </div>
                            @endif
                        </div>
                        
                    </div>
                    {{Form::hidden('metting_id',$metting->id)}}
                </div>

                <!--Footer-->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-outline-success btn-rounded btn-md ml-4" id="btnGuardar">{{trans('metting.save')}}</button>
                    <button type="button" class="btn btn-outline-danger btn-rounded btn-md ml-4" data-dismiss="modal">{{trans('metting.close_fields')}}</button>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>

@section('scripts')
    <script>
        $( document ).ready(function() {
        	$('option').mousedown(function(e) {
		    e.preventDefault();
		    $(this).prop('selected', $(this).prop('selected') ? false : true);
		    return false;
		});
            $( "#frmNewCommitments" ).submit(function( event ) {
                $.ajax({
                    url: '{{ url('/commitments') }}',
                    type: 'POST',
                    data: $("#frmNewCommitments").serialize(),
                    success: function(data) {
                        console.log(data);
                        if(data.status){
                            $("#descriptions").val("");
                            $('#commitmets').multiSelect('deselect_all');
                        }

                    },
                    error:function(){

                    },
                    beforeSend:function() {
                        $('#loader-wrapper').show();
                        $('#compromisoModal').modal('hide');
                        $('#modalconfirm').modal('show');
                    },
                    complete: function(){
                        setInterval(function(){
                           $('#loader-wrapper').hide();
                        },1000)
                    }
                });
                event.preventDefault();
            });
        });

    </script>

@endsection

@endif


<!-- Central Modal Large Info-->
<!--Modal: modalPush-->
<div class="modal fade" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
    <div class="modal-dialog modal-notify modal-info" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">Adjuntar Archivos</p>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="panel-body">
                    <form action="/upload/{!! $metting->id !!}" class="dropzone" id="my-dropzone">
                        {!! csrf_field() !!}
                    </form>

                </div>
            </div>

            <!--Footer-->
            <div class="d-flex justify-content-end">
                <button type="button" class="btn btn-outline-danger btn-rounded btn-md ml-4"
                data-dismiss="modal">{{ trans('metting.close_fields') }}</button>
            </div>
    
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: modalPush-->
<!--Modal: modalPush-->
<div class="modal fade" id="lista_compromiso" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
    <div class="modal-dialog modal-lg modal-notify modal-info" role="document">
        <!--Content-->
        <div class="modal-content text-center">
            <!--Header-->
            <div class="modal-header d-flex justify-content-center">
                <p class="heading">{{ trans('metting.list_commitments') }}</p>
            </div>

            <!--Body-->
            <div class="modal-body">
                <table class="table table-striped" id="tblGrid">
                    <thead id="tblHead">
                    <tr>
                        <th class="text-center">{{ trans('metting.user') }}</th>
                        <th class="text-center">{{ trans('metting.expiration_date') }}</th>
                        <th class="text-center">{{ trans('metting.description') }}</th>
                        <th class="text-center">{{ trans('metting.advance') }}</th>
                        <th class="text-center">{{ trans('metting.status') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($past_commitments as $past)
                        <tr>
                            <td style="width:50px" class="text-justify">{!!$past->name!!}</td>
                            <td style="width:50px" class="text-center">{!! \Carbon\Carbon::parse($past->end_date)->format('d/m/Y')!!}</td>
                            <td style="width:50px" class="text-justify">{!!$past->descriptions!!}</td>
                            <td style="width:50px" class="text-justify">{!!$past->progress!!}</td>
                            <td style="width:50px" class="text-center">
                                @if ($past->status == 1)
                                    <i class="fa fa-check-circle" style="color: #0d904f;text-align:center"></i> {{ trans('metting.accomplished') }}
                                @else
                                    <i class="fa fa-times-circle" style="color: red;text-align:center"></i> {{ trans('metting.unrealized') }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <!--Footer-->
            <div class="d-flex justify-content-end">
                <button type="button" class="btn btn-outline-danger btn-rounded btn-md ml-4"
                data-dismiss="modal">Cerrar</button>
            </div>
    
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: modalPush-->

{{-- 
<div class="modal fade" id="lista_compromiso">
    <div class="modal-dialog" style="max-width:95%!important">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">{{ trans('metting.list_commitments') }}</h3>
            </div>
            <div class="modal-body">
                <table class="table table-striped" id="tblGrid">
                    <thead id="tblHead">
                    <tr>
                        <th class="text-center">{{ trans('metting.user') }}</th>
                        <th class="text-center">{{ trans('metting.expiration_date') }}</th>
                        <th class="text-center">{{ trans('metting.description') }}</th>
                        <th class="text-center">{{ trans('metting.advance') }}</th>
                        <th class="text-center">{{ trans('metting.status') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($past_commitments as $past)
                        <tr>
                            <td style="width:50px" class="text-justify">{!!$past->name!!}</td>
                            <td style="width:50px" class="text-center">{!! \Carbon\Carbon::parse($past->end_date)->format('d/m/Y')!!}</td>
                            <td style="width:50px" class="text-justify">{!!$past->descriptions!!}</td>
                            <td style="width:50px" class="text-justify">{!!$past->progress!!}</td>
                            <td style="width:50px" class="text-center">
                                @if ($past->status == 1)
                                    <i class="fa fa-check-circle" style="color: #0d904f;text-align:center"></i> {{ trans('metting.accomplished') }}
                                @else
                                    <i class="fa fa-times-circle" style="color: red;text-align:center"></i> {{ trans('metting.unrealized') }}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default " data-dismiss="modal">{{ trans('metting.close_fields') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
--}}
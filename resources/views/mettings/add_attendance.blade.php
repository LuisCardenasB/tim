@if(isset($users))
    <!-- Full Height Modal Right Success Demo-->
    <div class="modal fade right" id="addAttendaceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true" data-backdrop="false">
        <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
            <!--Content-->
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header" style="background-color: #9c1f00 !important;">
                    <p class="heading lead" style="color: #f7f7f7; font-size: 25px;">{{trans('metting.list_of_assistants_add_attendance')}}</p>

                    <button type="button" class="close" style="color: #f7f7f7;" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <!--Body-->
                <div class="modal-body">
                    
                    <ul class="list-group z-depth-0">
                        @foreach($attendances as $key => $value)
                                <li class="list-group-item justify-content-between">
                                    {{$value->prefix}} {{$value->first_name}} {{$value->last_name}}
                                </li>
                        @endforeach
                    </ul>
                    <div >
                        <p class="heading" style="color: #777">{{trans('metting.select_attendees')}}
                        </p>
                    </div>
                    <div class="form-group" style="margin-top: 15px;">
                        <select name="attendances[]" class=" mdb-select md-form" multiple="multiple" id="attendances" style="width: 100%;"></select>
                    </div>
                    
                    {{--    <div class="md-form">
                        <input type="search" id="attendance" class="form-control mdb-autocomplete">
                        <label for="form-autocomplete" class="active">Agregar asistente</label>
                    </div>--}}
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-success btn-lg waves-effect waves-light" id="btnAddAttendence">{{trans('metting.add_attendees')}}
                    </button>
                </div>
                <!--Footer-->
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!-- Full Height Modal Right Success Demo-->
@section('scripts3')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>

        $( document ).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $( "#btnAddAttendence" ).on('click',function() {
                $.ajax({
                    url: '{{ url('/add_attendace_metting/'.$metting->id) }}',
                    type: 'PUT',
                    data: {attendaces: $("#attendances").val()},
                    success: function(data) {
                        if(data.status){
                            $('#addAttendaceModal').modal('hide');
                            location.reload(true);
                        }

                    }
                });
            });

            $('#attendances').select2({
                // Activamos la opcion "Tags" del plugin
                tags: true,
                tokenSeparators: [','],
                ajax: {
                    dataType: 'json',
                    url: '{{url('api/get_users')}}',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term,
                            id: {{$metting->id}}
                        }
                    },
                    processResults: function (data, page) {
                        return {
                            results: data
                        };
                    },
                }
            });
        });

    </script>

@endsection

@endif
@push('before_styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
@endpush
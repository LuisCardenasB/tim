<div class="row">
    <div class="col-md-8">
        <div class="card card-default">
            <div class="card-header">{{trans('metting.panel-heading')}}</div>
            <div class="card-body">
                <div class="row">
                    @if(isset($metting))
                        <div class="col-md-12">
                            {!! Form::hidden('user_id',Auth::id())!!}
                            <div class="md-form">
                                {!! Form::text('title',null,["class"=>"form-control",'placeholder'=>"ejemplo: Reunión anual", "disabled"=>"disabled"])!!}
                                <label for="form3" class="active">{{trans('metting.title_of_the_meeting_fields')}}</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6 col-sm-3 ">
                            <div class="md-form">
                                <input type="datetime-local" placeholder="ejemplo: dd/mm/yyyy" class="form-control" name="date" value="{{\Carbon\Carbon::parse($metting->date)->format('Y-m-d\TH:i')}}" disabled="disabled">
                                <label for="form3" class="active">{{trans('metting.date_fields')}}</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6 col-sm-3 ">
                            <div class="md-form">
                                {!! Form::text('location',null,["class"=>"form-control",'placeholder'=>"ejemplo: Lugar de la reunión","disabled"=>"disabled"])!!}
                                <label for="form3" class="active">{{trans('metting.place_fields')}}</label>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-6 col-sm-3 ">
                        <div class="md-form">
                            {!! Form::text('owner',null,["class"=>"form-control",'placeholder'=>"ejemplo: Juan Perez","disabled"=>"disabled"])!!}
                            <label for="form3" class="active">{{trans('metting.responsible_for_the_meeting_fields')}}</label>
                        </div>
                    </div>
                    @else
                        <div class="col-md-12">
                            {!! Form::hidden('user_id',Auth::id())!!}
                            <div class="md-form">
                                {!! Form::text('title',null,["class"=>"form-control",'placeholder'=>"ejemplo: Reunión anual"])!!}
                                <label for="form3" class="active">{{trans('metting.title_of_the_meeting_fields')}}</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6 col-sm-3 ">
                            <div class="md-form">
                                @if(isset($metting))
                                    <input type="datetime-local" placeholder="ejemplo: dd/mm/yyyy" class="form-control" name="date" value="{{\Carbon\Carbon::parse($metting->date)->format('Y-m-d\TH:i')}}">
                                @else
                                    <input type="datetime-local" placeholder="ejemplo: dd/mm/yyyy"  class="form-control" name="date" value="{{\Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y-m-d\TH:i')}}">
                                @endif
                                <label for="form3" class="active">{{trans('metting.date_fields')}}</label>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-6 col-sm-3 ">
                            <div class="md-form">
                                {!! Form::text('location',null,["class"=>"form-control",'placeholder'=>"ejemplo: Lugar de la reunión"])!!}
                                <label for="form3" class="active">{{trans('metting.place_fields')}}</label>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-6 col-sm-3 ">
                            <div class="md-form">
                                {!! Form::text('owner',null,["class"=>"form-control",'placeholder'=>"ejemplo: Juan Perez"])!!}
                                <label for="form3" class="active">{{trans('metting.responsible_for_the_meeting_fields')}}</label>
                            </div>
                        </div>
                    @endif
                    @if(env('METTING_LABEL_FOOTER') != false)
                        <div class="col-md-12 col-xs-6 col-sm-3 ">
                            <div class="md-form">
                                <?php
                                    \Carbon\Carbon::setLocale('es');
                                    if(!isset($metting->id)){
                                        $date_emmit = \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('F Y');
                                    }else{
                                        $date_emmit = \Carbon\Carbon::parse($metting->date)->format('F Y');
                                    }

                                ?>
                                <label for="form3" class="active"><strong>{{trans('metting.code_fields')}}</strong>
                                <!--{{$revision->code}}--> {{trans('metting.code')}} {{$revision->description}}, <strong>
                                {{trans('metting.date_of_issue_fields')}}</strong> {{$date_emmit}}</label>
                            </div>
                        </div>
                    @endif
                    @if(isset($metting->id))
                        <div class="col-md-12 col-xs-6 col-sm-3 ">
                            <button class="btn btn-outline-danger btn-rounded btn-md ml-4 btn_top" style="right: 30px;" type="submit" id="btncloseMetting">{{trans('metting.close_metting')}}</button>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        @if(!isset($metting->id) && env('AGENDA_HIDE_TEXTAREA') == false)
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{trans('metting.order_of_the_day_fields')}}</h3> {{trans('metting.topics_of_the_meeting_fields')}}
                </div>
                <div class="panel-body">
                    {!! Form::textarea('agenda',null,["class"=>"form-control ckeditor","id"=>"agenda"])!!}
                </div>
            </div>
        @else
            @if(env('AGENDA_HIDE_TEXTAREA') == true)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{trans('metting.order_of_the_day_fields')}}</h3> {{trans('metting.topics_of_the_meeting_fields')}}
                    </div>
                    <div class="panel-body">
                        {!! Form::textarea('agenda',null,["class"=>"form-control ckeditor","id"=>"agenda"])!!}
                    </div>
                </div>
            @endif
        @endif

        @if(isset($metting->id))
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">{{trans('metting.topic_fields')}}</h3> {{trans('metting.development_of_the_meeting_fields')}}
                </div>
                <div class="panel-body">
                    {!! Form::textarea('minutes',null,["class"=>"form-control ckeditor","id"=>"minutes","rows"=>"12","cols"=>"54"])!!}
                </div>
            </div>
        @endif

    </div>
    <div class="col-md-4">
        <div class="card card-default">
            <div class="card-header">{{trans('metting.participants')}}</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        @if(!isset($metting->id))
                            <div class="form-group">
                                {{ Form::select('users[]', $users, null, ['multiple' => true, 'class' => 'form-control','id'=>"commitmets"]) }}
                            </div>
                        @else
                        
                            @if(isset($users))
                                <table class="table">
                                    <tbody>
                                    @foreach($attendence as $key => $user)
                                       <!--  <li>
                                           {{$key+1}} - {{$user->prefix}} {{$user->first_name}} {{$user->last_name}}
                                        </li> -->
                                        <tr>
                                            <td>{{$user->prefix}} {{$user->first_name}} {{$user->last_name}}</td>
                                    @endforeach
                                 </tbody>
                                </table>

                            @endif
                            <div class="row">
                                <div class="col-md-6">
                                    <button class="btn btn-outline-primary btn-rounded btn-md ml-4" type="button" data-toggle="modal" data-target="#attendaceModal">{{trans('metting.list_fields')}}</button>
                                </div>
                            </div>

                        @endif
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="row">
                    <div class="col-md-12 text-right btn_top" style="right: 150px;" >
                        @if(isset($metting->id))
                            {{--<button class="btn btn-green" style="opacity: 0.95" type="button" data-toggle="modal" data-target="#lista_compromiso"><i class="fa fa-list" aria-hidden="true"></i>{{trans('metting.list_commitments')}}</button>--}}
                            <button class="btn btn-outline-primary btn-rounded btn-md ml-4" style="opacity: 0.95" type="button" data-toggle="modal" data-target="#compromisoModal">{{trans('metting.commitments_fields')}}</button>
                            {{--<button type="button" class="btn btn-info" style="opacity: 0.95" onclick="save()"> <i class="fa fa-floppy-o" aria-hidden="true"></i> {{trans('metting.save_fields')}}</button>--}}
                            {{--<a href="{{ URL::to('pdf/'. $metting->id ) }}" style="opacity: 0.95" target="_blank" class="btn btn-default"> <i class="fa fa-print" aria-hidden="true"></i> {{trans('metting.to_print')}}</a>--}}
                            <a href="#" class="float" id="menu-share">
                                <i class="fa fa-share my-float"></i>
                            </a>
                            <ul class="ul">
                                <li>
                                    <a href="#" onclick="save()" data-toggle="tooltip" data-placement="left" title="{{trans('metting.save_fields_commitments')}}">
                                        <i class="fa fa-floppy-o my-float"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-toggle="modal" data-target="#lista_compromiso">
                                        <span data-toggle="tooltip" data-placement="left" title="{{trans('metting.list_commitments')}}">
                                            <i class="fa fa-list my-float"></i>
                                        </span>
                                    </a>
                                </li>
                                @if (env('FLOATING_UPLOAD_FILE') != false)
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#modalPush">
                                            <span data-toggle="tooltip" data-placement="left" title="{{ trans('metting.upload_file') }}">
                                                <i class="fa fa-upload my-float"></i>
                                            </span>
                                        </a>
                                    </li>
                                @endif
                            </ul>

                        @else
                            <button class="btn btn-dark-green" type="submit">{{trans('metting.save_fields')}}</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



@push('after_styles')
    <link rel="stylesheet" href="{{asset('css/multi-select.css')}}"  media="screen" rel="stylesheet" type="text/css">

@endpush
@push('after_scripts')
    <script src="{{asset('js/jquery.multi-select.js')}}" type="text/javascript"></script>
    <script src="{{asset('js/quicksearch.js')}}" type="text/javascript"></script>

    <script>
        $(function(){
            $('#commitmets').multiSelect({
                selectableHeader: "<input type='text' class='search-input form-control col-md-12 col-xs-6 col-sm-3' style='margin-bottom: 15px;' autocomplete='off' placeholder=''>",
                selectionHeader: "<input type='text' class='search-input form-control col-md-12 col-xs-6 col-sm-3' style='margin-bottom: 15px;' autocomplete='off' placeholder=''>",
                afterInit: function(ms){
                    var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e){
                            if (e.which === 40){
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function(e){
                            if (e.which == 40){
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
                },
                afterSelect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                },
                afterDeselect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                }
            });
        });
    </script>
@endpush
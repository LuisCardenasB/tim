@extends('layouts.app')
@section('content')
    <section class="content">
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <h4>{{ trans('metting.please_fix') }}</h4>
                <ul>
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

            {!! Form::open(array('url' => '/mettings',  'id' => 'frmNewLetter')) !!}
            {!! csrf_field() !!}
            @include('mettings.fields')

            {!! Form::close() !!}
    </section>
    <!-- /.content -->
@endsection
@section('scripts')
    <script>
        $( document ).ready(function() {
        	$('option').mousedown(function(e) {
		    e.preventDefault();
		    $(this).prop('selected', $(this).prop('selected') ? false : true);
		    return false;
		});
        });

    </script>

@endsection
@extends('layouts.login')

@section('content')
<div class="login-page">
  <div class="form">
      <img class="logo_login" src="{{ asset('images/'.env("IMAGE_UPDATE_LOGIN").'') }}">
        <form class="login-form" role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
                 <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Username" required autofocus>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                                
                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                <button type="submit">Sign in</button>
                <p class="message">  
                    <a href="{{ route('password.request') }}">{{trans('passwords.forgot_your_password')}}</a>
                </p>
    </form>
  </div>
<div class="footer" role="contentinfo">
<p>Powered by TIM Groupware</p>
</div>
</div>
@endsection

@extends('layouts.login')

@section('content')
<div class="login-page">
    <div class="form">
        <img class="logo_login" src="{{ asset('images/logo.png') }}">
        @if (session('status'))
            <div class="alert alert-info" style="margin: 10px 10px 10px 10px;">
                {{ session('status') }}
            </div>
        @endif
        <span style="display: block;margin-top: 5px;margin-bottom: 10px;color: #737373;text-align: justify;">{{trans('passwords.You_can_reset_your_password_here')}}</span>
        <form class="login-form" role="form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="input-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <input id="email" type="email" placeholder="{{trans('passwords.email_address')}}" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <button type="submit" class="btn  btn-block">{{trans('passwords.send_email')}}</button>
            <br>
            <p class="message"> <a href="{{ asset('/login') }}" class="text-danger">{{trans('passwords.cancel')}}</a></p>
        </form>
    </div>
@endsection

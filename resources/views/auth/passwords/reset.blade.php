@extends('layouts.login')

@section('content')
<div class="login-page">
    <div class="form">
        <img class="logo_login" src="{{ asset('images/logo.png') }}">
        @if (session('status'))
            <div class="alert alert-info" style="margin: 10px 10px 10px 10px;">
                {{ session('status') }}
            </div>
        @endif
        @if ($errors->has('password_confirmation'))
            <div class="alert alert-danger" style="margin: 10px 10px 10px 10px;">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </div>
        @endif
        @if ($errors->has('email'))
            <div class="alert alert-danger" style="margin: 10px 10px 10px 10px;">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
        @if ($errors->has('password'))
            <div class="alert alert-danger" style="margin: 10px 10px 10px 10px;">
                <strong>{{ $errors->first('password') }}</strong>
            </div>
        @endif
        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}" autocomplete="off">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">{{trans('passwords.email_address')}}</label>
                <div>
                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">{{trans('passwords.password_reset')}}</label>
                <div>
                    <input id="password" type="password" class="form-control" name="password" required>
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm" class="control-label">{{trans('passwords.confirm_password')}}</label>
                <div>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>

            <div class="form-group">
                <div>
                    <button type="submit" class="btn">{{trans('passwords.reset_password')}}</button>
                </div>
            </div>
        </form>
    </div>
@endsection

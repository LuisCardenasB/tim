<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">


</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="panel-title"></h3></div>
                            <div class="col-md-6 text-right">

                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>Reunión de  {{$metting->title}} </h1>
                                <p>
                                    Llevada a cabo el día {{\Carbon\Carbon::parse($metting->date)->format('d-m-Y')}}
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>Asistentes <small>Pase de lista</small></h3></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul>
                                                    @foreach($attendances as $key => $value)
                                                        @if($value->status == 1)
                                                            <li><i class="glyphicon glyphicon-ok" style="color: #0d904f"></i> {{$value->name}}</li>
                                                        @else
                                                            <li><i class="glyphicon glyphicon-remove" style="color: red;"></i> {{$value->name}}</li>
                                                        @endif
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>Compromisos <small>Compromisos por asistente</small></h3></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr >
                                                            <th class="text-center">Asistente </th>
                                                            <th class="text-center">Compromiso </th>
                                                            <th class="text-center">Fecha </th>
                                                            <th class="text-center">Estado</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($commitments as $key => $value)
                                                            <tr>
                                                                <td>{{$value->name}}</td>
                                                                <td>{{$value->descriptions}}</td>
                                                                <td>{{\Carbon\Carbon::parse($value->end_date)->format('d-m-Y')}}</td>
                                                                <td class="text-center">
                                                                    @if($value->status == 1)
                                                                        <i class="glyphicon glyphicon-ok" style="color: #0d904f"></i>
                                                                    @else
                                                                        <i class="glyphicon glyphicon-remove" style="color: red;"></i>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2>Agenda <small>Topicos de la reunión</small></h2></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>{!! $metting->agenda !!} </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2>Minuta <small>Desarrollo de la reunión</small></h2></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>{!! $metting->minutes!!} </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h2>Conclusiones <small>Conclusiones para el cierre de la reunión</small></h2></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p>{!! $metting->conclusions!!} </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
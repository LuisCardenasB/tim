@extends('layouts.app')

@section('content')
  <!--  <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">COMPROMISOS</div>

                    <div class="panel-body">
                        @if (Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <table class="table table-striped table-bordered">-->
<div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Compromisos</h4>
                                <p class="category">Mis Compromisos</p>
                                <div id="_token" class="hidden" data-token="{{ csrf_token() }}"></div>
                            </div>
                            <div class="content table-responsive table-full-width">
                           <table class="table table-hover" style="    border-bottom: none;" id="commitment-table">
                            <thead>
                            <tr>
                                <th style="border-bottom-color: #fafafa;">ID</th>
                                <th style="border-bottom-color: #fafafa;">ESTADO</th>
                                @if(Auth::user()->isAdmin())
                                    <th style="border-bottom-color: #fafafa;">USUARIO</th>
                                @endif
                                <th style="border-bottom-color: #fafafa;">FECHA DE VENCIMIENTO</th>
                                <th style="border-bottom-color: #fafafa;">FECHA DE SEGUIMIENTO</th>
                                <th style="border-bottom-color: #fafafa;">DESCRIPCIÓN</th>
                                <th style="border-bottom-color: #fafafa;">REUNIÓN</th>
                                <th style="border-bottom-color: #fafafa;">AVANCE</th>
                                <th style="border-bottom-color: #fafafa;">ACCIÓN</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($commitments as $key => $value)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td class="text-center">
                                        @if($value->status == 1)
                                            <i class="pe-7s-check" style="color: #0d904f"></i>
                                        @else
                                            <i class="pe-7s-close-circle" style="color: red;"></i>
                                        @endif
                                        {{$value->status_commitments}}
                                    </td>
                                    @if(Auth::user()->isAdmin())
                                        {{--@if (Auth::user()->isAdmin())--}}
                                            {{--@if(isset($value->user->name))--}}
                                                {{--<td><a href="#" id="user" class="avance" data-source="" data-emptytext="{{ $value->user->name }}" data-value="{{ $value->user->name }}" data-type="select" data-pk="{{$value->user->id}}" data-url="/commitments_edit"></a></td>--}}
                                                {{--<td><a href="#" id="group" data-type="select" data-pk="{{$user->id}}" data-value="{{$user->id}}" data-source="{{asset('/user_edit')}}" data-title="Select group" class="editable editable-click" data-original-title="" title="" style="background-color: rgba(0, 0, 0, 0);">{{$user->name}}</a></td>--}}
                                            {{--@endif--}}
                                        {{--@else--}}
                                            @if(isset($value->user->name))
                                                <td>{{ $value->user->name }}</td>
                                            @endif
                                        {{--@endif--}}
                                    @endif

                                    @if (Auth::user()->isAdmin())
                                        <td><a href="#"
                                               id="date"
                                               class="avance"
                                               data-emptytext="{{ \Carbon\Carbon::parse($value->commitment->end_date)->format('d-m-Y')}}"
                                               data-value="{{ \Carbon\Carbon::parse($value->commitment->end_date)->format('d-m-Y')}}"
                                               data-type="combodate"
                                               data-format="DD-MM-YYYY"
                                               data-template="D MMM YYYY"
                                               data-pk="{{$value->commitment->id}}"
                                               data-url="/commitments_edit"></a>
                                        </td>
                                    @else
                                        <td>{{ \Carbon\Carbon::parse($value->commitment->end_date)->format('d-m-Y')}}</td>
                                    @endif

                                    @if(Auth::user()->isAdmin())
                                        <td><a href="#"
                                               id="date_tracking"
                                               class="avance"
                                               data-emptytext="{{ \Carbon\Carbon::parse($value->commitment->tracking_date)->format('d-m-Y')}}"
                                               data-value="{{ \Carbon\Carbon::parse($value->commitment->tracking_date)->format('d-m-Y')}}"
                                               data-type="combodate"
                                               data-format="DD-MM-YYYY"
                                               data-template="D MMM YYYY"
                                               data-pk="{{$value->commitment->id}}"
                                               data-url="/commitments_edit"></a>
                                        </td>
                                    @else
                                        <td>{{ \Carbon\Carbon::parse($value->commitment->tracking_date)->format('d-m-Y')}}</td>
                                    @endif

                                    @if(Auth::user()->isAdmin())
                                        <td><a href="#"
                                               id="description"
                                               class="avance"
                                               data-emptytext="{{ $value->commitment->descriptions }}"
                                               data-value="{{ $value->commitment->descriptions }}"
                                               data-type="text"
                                               data-pk="{{$value->commitment->id}}"
                                               data-url="/commitments_edit"></a>
                                        </td>
                                    @else
                                        <td>{{ $value->commitment->descriptions }}</td>
                                    @endif

                                    <td>
                                        {{ $value->commitment->metting->title }}
                                    </td>
                                    <td>
                                        @if($value->status == 1)
                                            {{$value->progress}}
                                        @elseif($value->user_id == Auth::user()->id)
                                            <a href="#"
                                               class="avance"
                                               data-emptytext="Agregar avance"
                                               data-value="{{$value->progress}}"
                                               data-type="textarea"
                                               rows="2"
                                               data-pk="{{$value->id}}"
                                               data-url="/avance"
                                               data-title="Agregar Avance"></a>
                                        @endif

                                    </td>

                                    <!-- we will also add show, edit, and delete buttons -->
                                    <td>

                                        <!-- delete the nerd (uses the destroy method DESTROY /nerds/{id} -->
                                        <!-- we will add this later since its a little more complicated than the other two buttons -->

                                        <!-- show the nerd (uses the show method found at GET /nerds/{id} -->
                                        {{--<a class="btn btn-small btn-success" href="{{ URL::to('mettings/' . $value->id) }}">VER</a>--}}

                                        <!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
                                        @if($value->status == 0 && $value->user_id == Auth::user()->id)
                                            {!! Form::model($value,array('route' => array('commitments.update',  $value->id),'method'=>'PUT','id'=>'frmUpdate')) !!}
                                            {!! csrf_field() !!}

                                            <button type="submit" class="btn btn-success">Realizado</button>
                                            {{Form::close()}}
                                        @endif


                                    </td>
                                </tr>
                            @endforeach
                                 </tbody>
                                </table>
                                {!! $commitments->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
            	.dataTables_filter{
            		    margin-bottom: 20px;
    					margin-right: 20px;
            	}
            </style>

@endsection
@push('after_scripts')
    <script>
        $(function() {
            $.fn.editable.defaults.mode = 'inline';
            $.fn.editableform.buttons = '<button type="submit" class="btn btn-info editable-submit"><i class="fa fa-check"></i></button>'+
                '<button type="button" class="btn btn-danger editable-cancel"><i class="fa fa-times"></i></button>';
            $.fn.editable.defaults.params = function (params) {
                params._token = $("#_token").data("token");
                return params;
            };
            $.fn.combodate.defaults = {
                minYear: {{\Carbon\Carbon::now()->year}},
                maxYear: {{\Carbon\Carbon::now()->year}},
            }

            $('.avance').editable();

            $('#date').editable();
            $('#date_tracking').editable();

            $('#user').editable();

            $("form").submit(function(e){
                var r = confirm("Al marcar el compromiso como realizado, no se podran agregar avances. \n\n¿Esta seguro de marcarlo como realizado?");
                if (r == true) {

                } else {
                    e.preventDefault();
                }

            });
            $.extend($.fn.dataTableExt.oStdClasses, {
			    "sFilterInput": "form-control yourClass",
			    "sLengthSelect": "form-control yourClass"
			});
            $('#commitment-table').DataTable({
                "paging": false,
            	"language": {
		            "lengthMenu": "Display _MENU_ records per page",
		            "zeroRecords": "No se encuentran registros",
		            "info": "Mostrando pagina _PAGE_ de _PAGES_",
		            "infoEmpty": "No hay registros disponibles",
		            "infoFiltered": "(filtered from _MAX_ total records)",
		            "search":"Filtrar"
		        }
            });

        })
    </script>
@endpush
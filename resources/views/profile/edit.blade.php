@extends('layouts.app')

@section('content')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{trans('perfil.update_perfil')}}</h4>
                            </div>
                            <div class="content">
                                {!! Form::model($user->profile,array('route' => array('profile.update',  $user->id),'method'=>'PUT','id'=>'frmUpdate','enctype'=>'multipart/form-data')) !!}
                                {!! csrf_field() !!}
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="hidden" value="{{$user->id}}" id="id_user">
                                            <div class="form-group">
                                                <label>{{trans('perfil.name_perfil')}}</label>
                                                {!! Form::text('first_name',null,["class"=>"form-control","placeholder"=>"Nombres"])!!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{trans('perfil.last_name_perfil')}}</label>
                                                {!! Form::text('last_name',null,["class"=>"form-control","placeholder"=>"Apellidos"])!!}
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>{{trans('perfil.initials_perfil')}}</label>
                                                {!! Form::text('initial',null,["class"=>"form-control","placeholder"=>"Iniciales"])!!}
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>{{trans('perfil.title')}}</label>
                                                {!! Form::text('prefix',null,["class"=>"form-control","placeholder"=>"Título"])!!}
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">{{trans('perfil.email_perfil')}}</label>
                                                <input type="email" class="form-control" placeholder="Correo electrónico" readonly value="{{$user->email}}">
                                            </div>
                                        </div>
                                    </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('perfil.password_perfil')}}</label>
                                            {!! Form::password('password',["class"=>"form-control","placeholder"=>""])!!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>{{trans('perfil.confirm_passsword_perfil')}}</label>
                                            {!! Form::password('password_confirm',["class"=>"form-control","placeholder"=>""])!!}
                                        </div>
                                    </div>
                                </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{trans('perfil.institution_perfil')}}</label>
                                                {!! Form::text('company',null,["class"=>"form-control","placeholder"=>"Título"])!!}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>{{trans('perfil.position_perfil')}}</label>
                                                {!! Form::text('positions',null,["class"=>"form-control","placeholder"=>"Título"])!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>{{trans('perfil.address_perfil')}}</label>
                                                {!! Form::text('address',null,["class"=>"form-control","placeholder"=>"Título"])!!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{trans('perfil.city_perfil')}}</label>
                                                {!! Form::text('city',null,["class"=>"form-control","placeholder"=>"Título"])!!}
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{trans('perfil.state')}}</label>
                                                {!! Form::text('state',null,["class"=>"form-control","placeholder"=>"Título"])!!}
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>{{trans('perfil.postal_code')}}</label>
                                                {!! Form::text('cp',null,["class"=>"form-control","placeholder"=>"Título"])!!}
                                            </div>
                                        </div>
                                    </div>
                                    {{--<div class="row">--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="">{{trans('perfil.profile_picture')}}</label>--}}
                                                {{--{{ Form::file('avatar', ['class' => 'form-control-file']) }}--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6">--}}
                                            {{--<div class="form-group">--}}
                                                {{--<label for="">{{trans('perfil.electronic_signature')}}</label>--}}
                                                {{--{{ Form::file('firm', ['class'=> 'form-control-file', 'id' => 'firm']) }}--}}
                                                {{--<div id="store_image"></div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}

                                    <button type="submit" class="btn btn-info btn-fill pull-right">{{trans('perfil.to_update')}}</button>
                                    <div class="clearfix"></div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image" style="background-color: #9C1F00">
                                {{--<img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>--}}
                            </div>
                            <div class="content" id="prueba">
                                <div class="author">

                                         @if($user->profile->avatar == null)
                                             {{ Form::file('avatar', ['class'=> 'form-control-file', 'id' => 'perfil','accept' => 'image/png,image/jpg']) }}
                                             <label for="perfil" class="snip1566">
                                                 <img src="{{ asset('img/avatar_null.jpg') }}" alt="..."/>
                                                 <figcaption><i class="fa fa-edit"></i></figcaption>
                                             </label>
                                         @else
                                            {{ Form::file('avatar', ['class'=> 'form-control-file', 'id' => 'perfil','accept' => 'image/png,image/jpg']) }}
                                            <label for="perfil" class="snip1566">
                                                <img src="{{ asset('storage/'.$user->profile->avatar) }}" alt="..."/>
                                                <figcaption><i class="fa fa-edit"></i></figcaption>
                                            </label>
                                         @endif

                                          <h4 class="title">{{$user->profile->first_name}} {{$user->profile->last_name}}<br />
                                             <small>{{$user->email}}</small>
                                          </h4>
                                         @if($user->profile->firm != null)
                                             {{ Form::file('firm', ['class'=> 'form-control-file', 'id' => 'fichero','accept' => 'image/png,image/jpg']) }}
                                             <label for="fichero" class="snip1567" >
                                                 <img src="{{asset('storage/'.$user->profile->firm)}}" style="width: 100%">
                                                 <figcaption><i class="fa fa-edit"></i></figcaption>
                                             </label>
                                         @else
                                             {{ Form::file('firm', ['class'=> 'form-control-file', 'id' => 'fichero', 'accept' => 'image/png,image/jpg']) }}
                                             <label for="fichero" class="" >
                                                 <a class="btn btn-primary btn-fill">Ingresar una firma electronica</a>
                                             </label>
                                         @endif
                                             

                                </div>
                            </div>
                            <hr>
                            <div class="text-center">
                               {{-- <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>--}}

                            </div>
                        </div>
                    </div>

                </div>
            </div>
    @include('models.insertimageModal')
    @include('models.insertimageperfil')
@endsection
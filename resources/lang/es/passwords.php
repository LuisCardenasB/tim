<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password'                         => 'Las contraseñas deben contener al menos 6 caracters y coincidir.',
    'reset'                            => 'Su contraseña ha sido cambiada.',
    'sent'                             => 'Le hemos enviado un correo de cambio de contraseña!',
    'token'                            => 'El código de cambio de contraseña es inválido.',
    'user'                             => "No podemos encontrar un usuario con ese nombre.",
    'reset_password'                   => 'Restablecer la contraseña',
    'email_address'                    => 'Dirección de correo electrónico',
    'send_password'                    => 'Enviar contraseña',
    'cancel'                           => 'Cancelar',
    'forgot_password'                  => '¿Se te olvidó tu contraseña?',
    'You_can_reset_your_password_here' => 'Ingrese su correo para enviarle un link de recuperación de contraseña.',
    'password_reset'                   => 'Contraseña',
    'confirm_password'                 => 'Confirmar contraseña',
    'forgot_your_password'             => '¿Olvidaste tu contraseña?',
    'send_email'                       => 'Enviar correo electronico'

];
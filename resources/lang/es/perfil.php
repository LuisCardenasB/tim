<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'update_perfil'            => 'Actualizar Perfil',
    'name_perfil'              => 'Nombre',
    'last_name_perfil'         => 'Apellido',
    'initials_perfil'          => 'Iniciales',
    'title_perfil'             => 'Titulo',
    'email_perfil'             => 'Correo Electrónico',
    'password_perfil'          => 'Contraseña',
    'confirm_passsword_perfil' => 'Confirmar Contraseña',
    'institution_perfil'       => 'Institución',
    'position_perfil'          => 'Cargo',
    'address_perfil'           => 'Dirección',
    'city_perfil'              => 'Ciudad',
    'state'                    => 'Estado',
    'postal_code'              => 'Código Postal',
    'profile_picture'          => 'Imagen de perfil',
    'electronic_signature'     => 'Firma Electrónica',
    'to_update'                => 'Actualizar',
    'crop_and_insert_image'    => 'Recortar e insertar imagen',
    'close'                    => 'Cerrar',

];
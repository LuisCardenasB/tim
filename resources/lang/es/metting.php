<?php
/**
 * Created by PhpStorm.
 * User: irvis_000
 * Date: 17/08/2018
 * Time: 11:12 AM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'panel-heading'                      => 'Información de la reunión',
    'participants'                       => 'Participantes',
    'mettings'                           => 'Minutas',
    'metting'                            => 'MINUTA',
    'metting_lower'                      => 'Minuta',
    'commitments'                        => 'Compromisos',
    'close'                              => 'Cancelar',
    'save'                               => 'Guardar',
    'list_attendance'                    => 'Lista de Asistencia',
    'attendance'                         => 'Asistío',
    'no_attendance'                      => 'No Asistío',
    'check'                              => 'Realizado',
    'uncheck'                            => 'Pendiente',
    'expired'                            => 'Vencido',
    'development_comments_PDF'           => 'COMENTARIOS DEL DESARROLLO',
    'participants_PDF'                   => 'PARTICIPANTES',
    'name_PDF'                           => 'NOMBRE',
    'job_PDF'                            => 'PUESTO',
    'firm_PDF'                           => 'FIRMA',
    'metting_PDF'                        => 'Minuta',
    'agreements_PDF'                     => 'ACUERDOS',
    'activity_PDF'                       => 'ACTIVIDAD',
    'responsible_PDF'                    => 'RESPONSABLE',
    'commitment_date_PDF'                => 'FECHA COMPROMISO',
    'tracking_date_PDF'                  => 'FECHA SEGUIMIENTO',
    'documents_delivered_PDF'            => 'DOCUMENTOS ENTREGADOS',
    'observations_PDF'                   => 'OBSERVACIONES',
    'affair_PDF'                         => 'ASUNTO',
    'date_PDF'                           => 'FECHA',
    'meeting_bitacora_list'              => 'Bitácora de Reuniones',
    'new_meeting_list'                   => 'Nueva Reunión',
    'list_of_minutes_list'               => 'Listado de minutas',
    'id_table_list'                      => 'ID',
    'title_table_list'                   => 'Titulo',
    'date_table_list'                    => 'Fecha',
    'responsible_table_list'             => 'Responsable',
    'state_table_list'                   => 'Estado',
    'action_table_list'                  => 'Accion',
    'btn_see_table_list'                 => 'Ver',
    'btn_start_table_list'               => 'Iniciar reunión',
    'title_of_the_meeting_fields'        => 'Título de la reunión:',
    'date_fields'                        => 'Fecha:',
    'place_fields'                       => 'Lugar:',
    'responsible_for_the_meeting_fields' => 'Responsable de la reunión:',
    'code_fields'                        => 'Código:',
    'date_of_issue_fields'               => 'Fecha de emisión:',
    'close_fields'                       => 'Cerrar',
    'close_metting'                      => 'Cerrar Minuta',
    'order_of_the_day_fields'            => 'Orden del día',
    'topics_of_the_meeting_fields'       => 'Topicos de la reunión',
    'topic_fields'                       => 'Tema',
    'development_of_the_meeting_fields'  => 'Desarrollo de la reunión',
    'list_fields'                        => 'Lista',
    'commitments_fields'                 => 'Compromisos',
    'save_fields'                        => 'Guardar',
    'save_fields_commitments'            => 'Guardar Minuta',
    'to_print'                           => 'Imprimir',
    'send_minutes'                       => 'Enviar minuta',
    'add_guests'                         => 'Agregar asistentes',
    'responsible'                        => 'Responsables',
    'tracking_date'                      => 'Fecha de seguimiento',
    'list_of_assistants_add_attendance'  => 'Lista de asistentes',
    'select_attendees'                   => 'Seleccionar asistentes:',
    'add_attendees'                      => 'Agregar asistentes',
    'modal_title'                        => 'Configuración',
    'copy_to'                            => 'Copia a:',
    'please_fix'                         => 'Llenar los campos requeridos',
    'code'                               => 'CF0013',
    'delete'                             => 'Eliminar',
    'revision'                           => 'Revisión B',
    'date_revision'                      => 'Fecha de emisión: Febrero 2019',
    'list_commitments'                   => 'Lista de Compromisos',
    'list_of_commitments_of_the'         => 'Lista de compromisos de la',
    'user'                               => 'Usario',
    'expiration_date'                    => 'Fecha de Vencimiento',
    'description'                        => 'Descripción',
    'advance'                            => 'Avance',
    'status'                             => 'Estatus',
    'accomplished'                       => 'Realizado',
    'unrealized'                         => 'No Realizado',
    'conclusion'                         => 'Conclusión',
    'attached'                           => 'Adjuntos',
    'upload_file'                        => 'Adjuntar archivos',
    'Do_you_want_to_add_a_new_commitment?' => '¿Desea agregar un nuevo compromiso?',
    'yes'                                => 'Si',
    'no'                                 => 'No'
];
<?php

return [
    'users_index'              => 'Usuarios',
    'list_of_users_index'      => 'Listado de usuarios',
    'new_user_index'           => 'Nuevo Usuario',
    'id_user_index'            => 'ID',
    'name_user_index'          => 'Nombre',
    'job_user_index'           => 'Puesto',
    'company_user_index'       => 'Empresa',
    'action'                   => 'Accion',
    'edit'                     => 'Editar',
    'administrator_permission' => 'Permiso de administrador',
    'user_update'              => 'Actualizar usuario',
];
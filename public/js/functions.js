/**
 * Created by irvis on 26/02/2017.
 */
function getNotificactions(id){
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: location.origin+'/notifications',
        type: 'GET',
        data: {},
        success: function(data) {
            if(data.length > 0){
                $("#listNotification").html("");
                $("#noNotifications").html(data.length);
                $("#noNotifications2").html(data.length);
                for(var i = 0; i < data.length; i++){
                    var html = '<li><a href="/commitments">'+data[i].descriptions+'</a></li>';
                    $("#listNotification").append(html);
                }
                if(data.length == 0){
                    $("#listNotification").html('<li><a href="./commitments">No existen compromisos pendientes</a></li>');
                }
            }
            setTimeout(function(){
                getNotificactions();
            },300000)
        },
        beforeSend:function() {

        },
        complete: function(){

        },error:function(e){
            console.log(e);
        }
    });
}

setTimeout(function(){
    getNotificactions();
},300000)

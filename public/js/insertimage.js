$(document).ready(function(){

    // Este apartado es para la imagen de la firma
    $image_crop = $('#image_demo').croppie({
        enableExif: true,
        viewport: {
            width:275,
            height:158,
            type:'square' //circle
        },
        boundary:{
            width:300,
            height:300
        }
    });

    $('#fichero').on('change', function(){
        var reader = new FileReader();
        reader.onload = function (event) {
            $image_crop.croppie('bind', {
                url: event.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
        $('#insertimageModal').modal('show');
    });

    $('.crop_image').click(function(event){
        var id = document.getElementById("id_user").value;
        $image_crop.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function(response){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'/profile/update_image',
                type:'POST',
                data:{
                    "image":response,
                    "id": id
                },
                success:function(data){
                    if (data.status == true){
                        $('#insertimageModal').modal('hide');
                        $("#prueba").load("../profile #prueba");
                    }
                }
            })
        });
    });
});
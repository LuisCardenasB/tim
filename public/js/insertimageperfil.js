$(document).ready(function () {
    // Este apartado es para la imagen de perfil de usuario
    $image_perfil = $('#image_perfil').croppie({
        enableExif: true,
        viewport: {
            width: 126,
            height: 126,
            type: 'circle' //square
        },
        boundary:{
            width:300,
            height:300
        }
    });

    $('#perfil').on('change', function(){
        var r = new FileReader();
        r.onload = function (e) {
            $image_perfil.croppie('bind', {
                url: e.target.result
            }).then(function () {
                console.log('JQuery bind complete');
            });
        }
        r.readAsDataURL(this.files[0]);
        $('#insertimagePerfil').modal('show');
    });

    $('.crop_image_perfil').click(function (event) {
        var id = document.getElementById("id_user").value;
        $image_perfil.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (response) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:'/profile/update_perfil',
                type:'POST',
                data:{
                    "image":response,
                    "id": id
                },
                success:function(data){
                    if (data.status == true)
                    {
                        $('#insertimagePerfil').modal('hide');
                        $("#prueba").load("../profile #prueba");
                    }
                }
            })
        })
    })

})
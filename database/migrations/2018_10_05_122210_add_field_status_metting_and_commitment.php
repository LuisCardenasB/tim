<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldStatusMettingAndCommitment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mettings', function (Blueprint $table) {
            //
            $table->integer('status_id')->default(1);
        });
        Schema::table('mettings_attendance', function (Blueprint $table) {
            //
            $table->integer('status')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mettings', function (Blueprint $table) {
            $table->dropColumn(['status']);
        });
        Schema::table('mettings_attendance', function (Blueprint $table) {
            $table->boolean('status')->change();
        });
    }
}

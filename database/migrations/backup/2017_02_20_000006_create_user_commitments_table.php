<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCommitmentsTable extends Migration
{
    /**
     * Run the migrations.
     * @table user_commitments
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_commitments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->unsignedInteger('commitment_id')->nullable()->default(null);
            $table->integer('status')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('commitment_id', 'fk_user_commitments_commitments1_idx')
                ->references('id')->on('commitments')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_id', 'fk_user_commitments_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('user_commitments');
     }
}

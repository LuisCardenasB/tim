<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMettingsTable extends Migration
{
    /**
     * Run the migrations.
     * @table mettings
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mettings', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('date')->nullable()->default(null);
            $table->string('title', 250)->nullable()->default(null);
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->timestamp('start_date')->nullable()->default(null);
            $table->text('agenda')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->text('minutes')->nullable()->default(null);
            $table->text('conclusions')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('user_id', 'fk_mettings_users1_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('mettings');
     }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     * @table user_profiles
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('avatar', 250)->nullable()->default(null);
            $table->string('positions', 25);
            $table->mediumText('description')->nullable()->default(null);
            $table->string('company', 150)->nullable()->default(null);
            $table->string('address', 150);
            $table->string('city', 50);
            $table->string('state', 50);
            $table->string('cp', 5);
            $table->string('initial', 5)->nullable()->default(null);
            $table->date('birthday')->nullable()->default(null);
            $table->string('prefix', 7)->nullable()->default(null);
            $table->nullableTimestamps();


            $table->foreign('user_id', 'fk_user_profile_users1_idx')
                ->references('id')->on('users')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('user_profiles');
     }
}

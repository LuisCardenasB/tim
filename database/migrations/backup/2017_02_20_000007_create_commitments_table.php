<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommitmentsTable extends Migration
{
    /**
     * Run the migrations.
     * @table commitments
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commitments', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('descriptions', 250)->nullable()->default(null);
            $table->timestamp('end_date')->nullable()->default(null);
            $table->unsignedInteger('metting_id')->nullable()->default(null);
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('metting_id', 'fk_commitments_mettings1_idx')
                ->references('id')->on('mettings')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('commitments');
     }
}

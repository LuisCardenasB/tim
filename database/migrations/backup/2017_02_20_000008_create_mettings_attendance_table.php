<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMettingsAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     * @table mettings_attendance
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mettings_attendance', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('mettings_id')->nullable()->default(null);
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default('0');
            $table->softDeletes();
            $table->nullableTimestamps();


            $table->foreign('mettings_id', 'fk_mettings_attendance_mettings1_idx')
                ->references('id')->on('mettings')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('user_id', 'fk_mettings_attendance_users1_idx')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists('mettings_attendance');
     }
}

-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 20-03-2017 a las 22:18:22
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 7.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tim`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `attachments`
--

CREATE TABLE `attachments` (
  `id` int(11) NOT NULL,
  `path` varchar(250) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `mettings_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `attachments`
--

INSERT INTO `attachments` (`id`, `path`, `user_id`, `mettings_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(18, 'attachments/8LsZLs0OwkEktRtbvbGObr0fwRHmeYOVKdkvsmff.jpeg', 4, 2, '2017-03-20 22:15:57', '2017-03-20 22:15:57', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `commitments`
--

CREATE TABLE `commitments` (
  `id` int(10) UNSIGNED NOT NULL,
  `descriptions` varchar(250) DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `metting_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `commitments`
--

INSERT INTO `commitments` (`id`, `descriptions`, `end_date`, `metting_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'dasfdsa', '2017-03-16 07:00:00', 3, '2017-03-17 00:23:47', '2017-03-17 00:23:47', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mettings`
--

CREATE TABLE `mettings` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `agenda` text,
  `description` text,
  `minutes` text,
  `conclusions` text,
  `status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mettings`
--

INSERT INTO `mettings` (`id`, `date`, `title`, `user_id`, `start_date`, `agenda`, `description`, `minutes`, `conclusions`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2017-02-27 22:29:00', 'dfsadf', 4, NULL, NULL, NULL, NULL, NULL, 0, '2017-02-27 22:30:06', '2017-02-27 22:30:06', NULL),
(2, '2017-02-28 16:36:00', 'sghfsdg', 4, NULL, NULL, NULL, NULL, NULL, 0, '2017-02-28 16:36:40', '2017-02-28 16:36:40', NULL),
(3, '2017-03-16 22:48:00', 'fgvsdfg', 4, NULL, '<p>ghgfd</p>', NULL, '<p>hfdg</p>', '<p>dfgh</p>', 3, '2017-03-17 00:23:15', '2017-03-17 00:24:42', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mettings_attendance`
--

CREATE TABLE `mettings_attendance` (
  `id` int(10) UNSIGNED NOT NULL,
  `mettings_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mettings_attendance`
--

INSERT INTO `mettings_attendance` (`id`, `mettings_id`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 4, 0, '2017-02-27 22:30:06', '2017-02-27 22:30:06', NULL),
(2, 2, 4, 0, '2017-02-28 16:36:40', '2017-02-28 16:36:40', NULL),
(3, 3, 4, 1, '2017-03-17 00:23:15', '2017-03-17 00:23:53', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `description`, `group`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'Administrador', 'default', NULL, NULL),
(2, 'user', 'user', 'usuario normal', 'default', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 4, NULL, NULL),
(2, 2, 12, '2017-03-20 21:44:57', '2017-03-20 21:44:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'Irvis', 'irvis_isaac@hotmail.com', '$2y$10$GkY8LO6Nid.m7v45PwcK8eB2NyTS1J93TR0lJJYeWXp0t.J9hYSMi', 'B0JeQeyDr41yv2phvcawI14nckW0fTeyhDIMZIj9ycRcGZ9f0nXEmBFj2Nhn', '2017-02-27 19:04:07', '2017-02-27 19:04:07'),
(5, 'Eduardo Hernandez', 'a@b.com', '$2y$10$LwJ6WUvQTM0k7f0pkz8WF.BU4a3h41fAkTPNBNJkbklEo9/OF5qhi', NULL, '2017-03-20 10:48:55', '2017-03-20 10:48:55'),
(7, 'irvis isaac ozuna castillo', 'irvis_isaac@gmail.com', '$2y$10$9LCjpUMKIW5rceX1ZQXqGuviqJxSV.9UWop.omzoG5OHLgVyAMpPu', NULL, '2017-03-20 10:51:45', '2017-03-20 10:51:45'),
(11, 'irvis isaac ozuna castillo', 'irvias_isaaasc@gmail.com', '$2y$10$KZBsUFzB9Ui2b2zkVpMn3ul66uraG2P.LjHh1eJ5hJtHAkevwccTy', NULL, '2017-03-20 10:53:20', '2017-03-20 10:53:20'),
(12, 'Javier centeno', 'irvis_isaaaasc@gmail.com', '$2y$10$XfAu571zu53Y.M6sJa5vNueooOw2XDNSdYJsDuJOxNhad5QfBk50S', NULL, '2017-03-20 21:44:57', '2017-03-20 22:00:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_commitments`
--

CREATE TABLE `user_commitments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `commitment_id` int(10) UNSIGNED DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_commitments`
--

INSERT INTO `user_commitments` (`id`, `user_id`, `commitment_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 4, 1, 1, '2017-03-17 00:23:47', '2017-03-17 00:25:10', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(10) NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `avatar` varchar(250) DEFAULT NULL,
  `positions` varchar(25) NOT NULL,
  `description` mediumtext,
  `company` varchar(150) DEFAULT NULL,
  `address` varchar(150) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `initial` varchar(5) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `prefix` varchar(7) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `first_name`, `last_name`, `avatar`, `positions`, `description`, `company`, `address`, `city`, `state`, `cp`, `initial`, `birthday`, `prefix`, `created_at`, `updated_at`) VALUES
(1, 4, 'Irvis Isaac', 'Ozuna Castillo', 'avatars/lsVON7vsazfCzKLGhnZfnO05yzWEKFbuYR5Ultet.jpeg', 'LIDER TECNICO', NULL, 'QUANTUMBIT', 'COLOSIO 294 F', 'HERMOSILLO', 'SONORA', '83116', 'IIOC', NULL, 'ING', NULL, '2017-03-18 16:52:12'),
(2, 5, 'Eduardo', 'Hernandez', NULL, 'Director', NULL, 'Quantumbit', 'Algo por ahi', 'Hermosillo', 'Sonora', '83120', 'EH', NULL, 'ING', '2017-03-20 10:48:55', '2017-03-20 10:48:55'),
(3, 7, 'irvis isaac', 'ozuna castillo', NULL, '123', NULL, 'Prueba', 'Conocido', 'Hermosillo', 'Select One', '83120', 'asd', NULL, 'as', '2017-03-20 10:51:45', '2017-03-20 10:51:45'),
(4, 11, 'irvis isaac', 'ozuna castillo', NULL, 'dsafs', NULL, 'Prueba', 'Conocido', 'Hermosillo', 'Select One', '83120', 'a', NULL, 'asd', '2017-03-20 10:53:20', '2017-03-20 10:53:20'),
(5, 12, 'Javier', 'centeno', 'avatars/dmcSoaxuQrBVc4IdObG87gBSTJxMEq88d5pi3LL5.jpeg', 'Gerente', NULL, 'Benavides', 'alguna', 'Nogales', 'SOnora', '83121', 'JC', NULL, 'Lic.', '2017-03-20 21:44:57', '2017-03-20 22:00:21'),
(6, 12, 'irvis isaac asd', 'castillo', NULL, 'dfvd', NULL, 'Prueba', 'Conocido', 'Hermosillo', 'Select One', '83120', 'IIOZ', NULL, 'sd', '2017-03-20 21:49:44', '2017-03-20 21:49:44'),
(7, 12, 'Francisco', 'Centeno', NULL, 'Gerente', NULL, 'Benavides', 'algo mas', 'Nogales', 'Sonora', '83121', 'FC', NULL, 'Lic.', '2017-03-20 21:51:31', '2017-03-20 21:51:31');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_attachments_users1_idx` (`user_id`),
  ADD KEY `fk_attachment_metting` (`mettings_id`) USING BTREE;

--
-- Indices de la tabla `commitments`
--
ALTER TABLE `commitments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_commitments_mettings1_idx` (`metting_id`);

--
-- Indices de la tabla `mettings`
--
ALTER TABLE `mettings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mettings_users1_idx` (`user_id`);

--
-- Indices de la tabla `mettings_attendance`
--
ALTER TABLE `mettings_attendance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_mettings_attendance_mettings1_idx` (`mettings_id`),
  ADD KEY `fk_mettings_attendance_users1_idx` (`user_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Indices de la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_role_id_index` (`role_id`),
  ADD KEY `role_user_user_id_index` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `user_commitments`
--
ALTER TABLE `user_commitments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_commitments_users1_idx` (`user_id`),
  ADD KEY `fk_user_commitments_commitments1_idx` (`commitment_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `commitment_id` (`commitment_id`);

--
-- Indices de la tabla `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user_profile_users1_idx` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `commitments`
--
ALTER TABLE `commitments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `mettings`
--
ALTER TABLE `mettings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `mettings_attendance`
--
ALTER TABLE `mettings_attendance`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `user_commitments`
--
ALTER TABLE `user_commitments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `attachments`
--
ALTER TABLE `attachments`
  ADD CONSTRAINT `fk_attachments_mettings1` FOREIGN KEY (`mettings_id`) REFERENCES `mettings` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_attachments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `commitments`
--
ALTER TABLE `commitments`
  ADD CONSTRAINT `fk_commitments_mettings1` FOREIGN KEY (`metting_id`) REFERENCES `mettings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mettings`
--
ALTER TABLE `mettings`
  ADD CONSTRAINT `fk_mettings_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `mettings_attendance`
--
ALTER TABLE `mettings_attendance`
  ADD CONSTRAINT `fk_mettings_attendance_mettings1` FOREIGN KEY (`mettings_id`) REFERENCES `mettings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_mettings_attendance_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `user_commitments`
--
ALTER TABLE `user_commitments`
  ADD CONSTRAINT `fk_user_commitments_commitments1` FOREIGN KEY (`commitment_id`) REFERENCES `commitments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_commitments_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD CONSTRAINT `fk_user_profile_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
